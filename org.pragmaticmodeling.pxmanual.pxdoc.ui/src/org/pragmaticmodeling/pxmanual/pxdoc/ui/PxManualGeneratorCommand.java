package org.pragmaticmodeling.pxmanual.pxdoc.ui;



import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.pragmaticmodeling.pxmanual.pxManual.Document;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

public class PxManualGeneratorCommand extends AbstractPxManualGeneratorCommand {
	
	
	@Override
	protected Object resolveInputModel(ExecutionEvent event) {
		Object input = super.resolveInputModel(event);
		if (input instanceof IFile) {
			IFile file = (IFile)input;
			ResourceSet rs = new ResourceSetImpl();
			//File file = new File(f);
			Resource resource = rs.getResource(URI.createFileURI(file.getLocation().toString()), true);
//			Resource resource = rs.getResource(URI.createURI("./my2.dmodel"), true);
			EObject eobject = resource.getContents().get(0);
			if (eobject != null) {
				input = eobject;
			}
		} 
		if (input instanceof EObject) {
			EObject model = EcoreUtil2.getContainerOfType((EObject) input, Document.class);
			if (model != null) {
				return model;
			}
		}
		return input;
	}
}
