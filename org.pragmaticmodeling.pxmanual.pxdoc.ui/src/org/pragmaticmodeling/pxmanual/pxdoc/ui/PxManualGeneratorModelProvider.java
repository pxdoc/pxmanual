package org.pragmaticmodeling.pxmanual.pxdoc.ui;

import org.eclipse.emf.ecore.EObject;
import org.pragmaticmodeling.pxmanual.pxManual.Document;

import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;

/**
 * Model Provider
 */
public class PxManualGeneratorModelProvider implements IModelProvider {

	@Override
	public Object adaptSelection(Object selection) {
		if (selection instanceof EObject) {
			EObject model = EcoreUtil2.getContainerOfType((EObject) selection, Document.class);
			if (model != null) {
				return model;
			}
		}
		return selection;
	}

}
	 
	
