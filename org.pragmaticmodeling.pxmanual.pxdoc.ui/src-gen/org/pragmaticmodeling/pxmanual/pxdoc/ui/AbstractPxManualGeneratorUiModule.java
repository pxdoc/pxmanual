package org.pragmaticmodeling.pxmanual.pxdoc.ui;

import org.eclipse.jface.dialogs.IDialogSettings;

import com.google.inject.Binder;
import com.google.inject.Module;

import org.pragmaticmodeling.pxmanual.pxdoc.PxManualGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocLanguageRenderersManager;
import fr.pragmaticmodeling.pxdoc.runtime.IResourceProvider;
import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.EclipseResourceProvider;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.StylesheetRegistryImpl;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.internal.language.renderers.EclipseLanguageRenderersManager;
import fr.pragmaticmodeling.pxdoc.runtime.eclipse.preferences.PxDocEclipsePreferences;
import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;
import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.IPxDocWizard;
import fr.pragmaticmodeling.pxdoc.runtime.preferences.IPxDocPreferences;

public abstract class AbstractPxManualGeneratorUiModule implements Module {
	
	private final AbstractPxUiPlugin plugin;
		   
	public AbstractPxManualGeneratorUiModule(AbstractPxUiPlugin plugin) {
		this.plugin = plugin;
	}
		
	@Override
	public void configure(Binder binder) {
		binder.bind(AbstractPxUiPlugin.class).toInstance(plugin);
		binder.bind(IDialogSettings.class).toInstance(plugin.getDialogSettings());
		binder.bind(IStylesheetsRegistry.class).to(StylesheetRegistryImpl.class);
		binder.bind(IPxDocLanguageRenderersManager.class).to(EclipseLanguageRenderersManager.class);
		binder.bind(IPxDocGenerator.class).to(PxManualGenerator.class);
		binder.bind(IResourceProvider.class).to(EclipseResourceProvider.class);
		binder.bind(IPxDocPreferences.class).to(PxDocEclipsePreferences.class);
		binder.bind(IModelProvider.class).to(PxManualGeneratorModelProvider.class);
		binder.bind(IPxDocWizard.class).to(PxManualGeneratorWizard.class);
	}
	
}	
