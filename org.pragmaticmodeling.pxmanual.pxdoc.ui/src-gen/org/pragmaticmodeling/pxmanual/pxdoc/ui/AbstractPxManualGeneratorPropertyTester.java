package org.pragmaticmodeling.pxmanual.pxdoc.ui;

import fr.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester;

/**
 * PxDocModel property tester and model provider
 */
public class AbstractPxManualGeneratorPropertyTester extends DefaultPropertyTester {

	public AbstractPxManualGeneratorPropertyTester() {
		super(java.lang.Object.class);
	}
}

