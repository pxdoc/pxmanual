package org.pragmaticmodeling.pxmanual.pxdoc;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import fr.pragmaticmodeling.pxdoc.Bookmark;
import fr.pragmaticmodeling.pxdoc.Cell;
import fr.pragmaticmodeling.pxdoc.ContainerElement;
import fr.pragmaticmodeling.pxdoc.HeadingN;
import fr.pragmaticmodeling.pxdoc.Italic;
import fr.pragmaticmodeling.pxdoc.Measure;
import fr.pragmaticmodeling.pxdoc.MergeKind;
import fr.pragmaticmodeling.pxdoc.Row;
import fr.pragmaticmodeling.pxdoc.ShadingPattern;
import fr.pragmaticmodeling.pxdoc.StyleApplication;
import fr.pragmaticmodeling.pxdoc.Table;
import fr.pragmaticmodeling.pxdoc.UnitKind;
import fr.pragmaticmodeling.pxdoc.VerticalAlignmentType;
import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocRendererExtension;
import fr.pragmaticmodeling.pxdoc.runtime.PxDocGenerationException;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxmanual.pxManual.AbstractExample;
import org.pragmaticmodeling.pxmanual.pxManual.AbstractSection;
import org.pragmaticmodeling.pxmanual.pxManual.DocBody;
import org.pragmaticmodeling.pxmanual.pxManual.Document;
import org.pragmaticmodeling.pxmanual.pxManual.Example;
import org.pragmaticmodeling.pxmanual.pxManual.Image;
import org.pragmaticmodeling.pxmanual.pxManual.Keyword;
import org.pragmaticmodeling.pxmanual.pxManual.KeywordAttribute;
import org.pragmaticmodeling.pxmanual.pxManual.ManualElement;
import org.pragmaticmodeling.pxmanual.pxManual.Page;
import org.pragmaticmodeling.pxmanual.pxManual.SamePage;
import org.pragmaticmodeling.pxmanual.pxManual.Section;
import org.pragmaticmodeling.pxmanual.pxManual.Text;
import org.pragmaticmodeling.pxmanual.pxManual.Usage;
import org.pragmaticmodeling.pxmanual.pxdoc.HtmlExtension;

@SuppressWarnings("all")
public class PxManualGenerator extends AbstractPxDocGenerator {
  private Logger logger = Logger.getLogger(getClass());
  
  public String modelDirectory;
  
  public IPxDocRendererExtension htmlExtension = new HtmlExtension(this.getGenerator());
  
  public PxManualGenerator() {
    super();
  }
  
  public void generate() throws PxDocGenerationException {
    org.pragmaticmodeling.pxmanual.pxManual.Document model = (org.pragmaticmodeling.pxmanual.pxManual.Document)launcher.getModel().getInstance();
    try {
    	logger.info("----------------------------------------------------------------");
    	logger.info("Starting PxManualGenerator generation...");
    	checkStylesheet(getStylesheetName());
    	main(null, model);
    } catch (Exception e) {
    	throw new PxDocGenerationException(e);
    } catch (NoClassDefFoundError e) {
        logger.error(e.getMessage());
    } finally {
    	logger.info("FINISHED: PxManualGenerator generation...");
    }
  }
  
  @Override
  public String getStylesheetName() {
    return "RefDoc";
  }
  
  public String trimLastNewLine(final String text) {
    String _xblockexpression = null;
    {
      String result = text;
      boolean _endsWith = text.endsWith("\n");
      if (_endsWith) {
        int _length = text.length();
        int _minus = (_length - 1);
        result = text.substring(0, _minus);
      }
      _xblockexpression = result;
    }
    return _xblockexpression;
  }
  
  public List<Example> getBasicExamples(final Keyword keyword) {
    return IterableExtensions.<Example>toList(Iterables.<Example>filter(keyword.getExamples(), Example.class));
  }
  
  public List<Usage> getUsageExamples(final Keyword keyword) {
    return IterableExtensions.<Usage>toList(Iterables.<Usage>filter(keyword.getExamples(), Usage.class));
  }
  
  public String toBookmarkId(final String rawName) {
    return null;
  }
  
  public File getProjectFile(final File directory) {
    File[] _listFiles = directory.listFiles();
    for (final File file : _listFiles) {
      String _name = file.getName();
      boolean _equals = Objects.equal(_name, ".project");
      if (_equals) {
        return file;
      }
    }
    return null;
  }
  
  public String getParentProjectDirectory(final File file) {
    File parentDir = file.getParentFile();
    if ((parentDir != null)) {
      File _projectFile = this.getProjectFile(parentDir);
      boolean _tripleNotEquals = (_projectFile != null);
      if (_tripleNotEquals) {
        return parentDir.getAbsolutePath();
      } else {
        return this.getParentProjectDirectory(parentDir);
      }
    } else {
      return null;
    }
  }
  
  public String getModelDirectory(final Document model) {
    String _xblockexpression = null;
    {
      String projectDir = null;
      URI uri = model.eResource().getURI();
      boolean _hasAbsolutePath = uri.hasAbsolutePath();
      if (_hasAbsolutePath) {
        File file = null;
        boolean _isPlatformResource = uri.isPlatformResource();
        if (_isPlatformResource) {
          IWorkspaceRoot lWR = ResourcesPlugin.getWorkspace().getRoot();
          String path = uri.toPlatformString(true);
          Path _path = new Path(path);
          IResource ifile = lWR.findMember(_path);
          String _string = ifile.getLocation().toString();
          File _file = new File(_string);
          file = _file;
        } else {
          String _fileString = uri.toFileString();
          File _file_1 = new File(_fileString);
          file = _file_1;
        }
        projectDir = this.getParentProjectDirectory(file);
      }
      _xblockexpression = projectDir;
    }
    return _xblockexpression;
  }
  
  public void error(final String message) {
    logger.error(message);
  }
  
  public void info(final String message) {
    logger.info(message);
  }
  
  public void toCitation(final ContainerElement parent, final Text element, final boolean html) throws PxDocGenerationException {
    Table lObj0 = createTable(parent, null, null, createBorder(null, null, 0), "192,192,192", ShadingPattern.NO_VALUE);
    Row lObj1 = createRow(lObj0, null, false, null, ShadingPattern.NO_VALUE, false, null);
    Cell lObj2 = createCell(lObj1);
    Italic lObj3 = createItalic(lObj2);
    if (html) {
      addAllToParent(getLanguageRenderer("html").render(lObj3, element.getBody(), this.htmlExtension), lObj3);
      
    } else {
      createText(lObj3, element.getBody());
    }
  }
  
  public void toText(final ContainerElement parent, final Text element) throws PxDocGenerationException {
    String _language = element.getLanguage();
    boolean _equals = Objects.equal(_language, "html");
    if (_equals) {
      String _className = element.getClassName();
      boolean _equals_1 = Objects.equal(_className, "note");
      if (_equals_1) {
        createParagraphBreak(parent);
        StyleApplication lObj0 = createStyleApplication(parent, false, "Quote", null, null, null);
        addAllToParent(getLanguageRenderer("html").render(lObj0, element.getBody(), this.htmlExtension), lObj0);
        
        createParagraphBreak(parent);
      } else {
        String _className_1 = element.getClassName();
        boolean _equals_2 = Objects.equal(_className_1, "citation");
        if (_equals_2) {
          toCitation(parent, element, true);
        } else {
          addAllToParent(getLanguageRenderer("html").render(parent, element.getBody(), this.htmlExtension), parent);
          
        }
      }
    } else {
      String _language_1 = element.getLanguage();
      boolean _equals_3 = Objects.equal(_language_1, "pxdoc");
      if (_equals_3) {
        addAllToParent(getLanguageRenderer("pxdoc").render(parent, this.trimLastNewLine(element.getBody()), null), parent);
        
      } else {
        String _className_2 = element.getClassName();
        boolean _equals_4 = Objects.equal(_className_2, "note");
        if (_equals_4) {
          createParagraphBreak(parent);
          StyleApplication lObj1 = createStyleApplication(parent, false, "Quote", null, null, null);
          createText(lObj1, element.getBody());
          createParagraphBreak(parent);
        } else {
          String _className_3 = element.getClassName();
          boolean _equals_5 = Objects.equal(_className_3, "citation");
          if (_equals_5) {
            toCitation(parent, element, false);
          } else {
            createText(parent, element.getBody());
            createParagraphBreak(parent);
          }
        }
      }
    }
  }
  
  public void toTitle(final ContainerElement parent, final AbstractSection element, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    toDoc(lObj0, element.getTitle());
    Iterator<org.pragmaticmodeling.pxmanual.pxManual.ManualElement> lObj1 = getIterator(element.getElements());
    while (lObj1.hasNext()) {
      toDoc(parent, lObj1.next(), (level + 1));
      
    }
  }
  
  public void toImage(final ContainerElement parent, final Image element) throws PxDocGenerationException {
    createImage(parent, ((this.modelDirectory + "/") + element.getPath()), null, null, null, null, null);
    createParagraphBreak(parent);
  }
  
  public void toBasicExample(final ContainerElement parent, final AbstractExample basicExample) throws PxDocGenerationException {
    Iterator<org.pragmaticmodeling.pxmanual.pxManual.ManualElement> lObj0 = getIterator(basicExample.getBody().getElements());
    while (lObj0.hasNext()) {
      toDoc(parent, lObj0.next());
      
    }
  }
  
  public void toDoc(final ContainerElement parent, final DocBody docBody, final int level) throws PxDocGenerationException {
    Iterator<org.pragmaticmodeling.pxmanual.pxManual.ManualElement> lObj0 = getIterator(docBody.getElements());
    while (lObj0.hasNext()) {
      toDoc(parent, lObj0.next(), level);
      
    }
  }
  
  public void toDoc(final ContainerElement parent, final DocBody docBody) throws PxDocGenerationException {
    if ((docBody != null)) {
      Iterator<org.pragmaticmodeling.pxmanual.pxManual.ManualElement> lObj0 = getIterator(docBody.getElements());
      while (lObj0.hasNext()) {
        toDoc(parent, lObj0.next(), (-1));
        
      }
    }
  }
  
  public void attributeRow(final ContainerElement parent, final KeywordAttribute attribute) throws PxDocGenerationException {
    Row lObj0 = createRow(parent, null, false, null, ShadingPattern.NO_VALUE, false, null);
    {
      Cell lObj1 = createCell(lObj0);
      Iterator<org.pragmaticmodeling.pxmanual.pxManual.AbstractExample> lObj2 = getIterator(attribute.getExample());
      while (lObj2.hasNext()) {
        toBasicExample(lObj1, lObj2.next());
        
      }
      Cell lObj3 = createCell(lObj0);
      createCellDef(lObj3, MergeKind.AUTOMATIC, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      toDoc(lObj3, attribute.getDocumentation());
      Cell lObj4 = createCell(lObj0);
      createCellDef(lObj4, MergeKind.AUTOMATIC, null, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      toDoc(lObj4, attribute.getPossibleValues());
      Cell lObj5 = createCell(lObj0);
      boolean _isOptional = attribute.isOptional();
      if (_isOptional) {
        createText(lObj5, "Yes");
      } else {
        createText(lObj5, "No");
      }
    }
  }
  
  public void toUsageExample(final ContainerElement parent, final Usage e) throws PxDocGenerationException {
    StyleApplication lObj0 = createStyleApplication(parent, true, "UsageOfpxDoc", null, null, null);
    createText(lObj0, e.getTitle().getBody());
    Iterator<org.pragmaticmodeling.pxmanual.pxManual.ManualElement> lObj1 = getIterator(e.getBody().getElements());
    while (lObj1.hasNext()) {
      toDoc(parent, lObj1.next());
      
    }
    createParagraphBreak(parent);
  }
  
  public void title(final ContainerElement parent, final Keyword keyword) throws PxDocGenerationException {
    Text _prettyTitle = keyword.getPrettyTitle();
    boolean _tripleNotEquals = (_prettyTitle != null);
    if (_tripleNotEquals) {
      toText(parent, keyword.getPrettyTitle());
    } else {
      createText(parent, "Keyword \'");
      createText(parent, keyword.getKeywordName());
      createText(parent, "\'");
    }
  }
  
  public void toKeyword(final ContainerElement parent, final Keyword element, final int level) throws PxDocGenerationException {
    HeadingN lObj0 = createHeadingN(parent, level, false);
    String lObj1 = element.getKeywordName();
    Bookmark lObj2 = createBookmark(lObj0, getBookmarkString(lObj1));
    title(lObj2, element);
    boolean _isEmpty = element.getAttributes().isEmpty();
    if (_isEmpty) {
      Iterator<org.pragmaticmodeling.pxmanual.pxManual.Example> lObj3 = getIterator(this.getBasicExamples(element));
      while (lObj3.hasNext()) {
        toBasicExample(parent, lObj3.next());
        if (lObj3.hasNext()) {
          createParagraphBreak(parent);
        }
      }
    }
    DocBody _documentation = element.getDocumentation();
    boolean _tripleNotEquals = (_documentation != null);
    if (_tripleNotEquals) {
      toDoc(parent, element.getDocumentation(), level);
    }
    boolean _isEmpty_1 = element.getAttributes().isEmpty();
    boolean _not = (!_isEmpty_1);
    if (_not) {
      Iterator<org.pragmaticmodeling.pxmanual.pxManual.Example> lObj4 = getIterator(this.getBasicExamples(element));
      while (lObj4.hasNext()) {
        toBasicExample(parent, lObj4.next());
        if (lObj4.hasNext()) {
          createParagraphBreak(parent);
        }
      }
      Table lObj5 = createTable(parent, null, null, null, null, ShadingPattern.NO_VALUE);
      Measure cellWidth6 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj5, null, cellWidth6, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth7 = createMeasure((float)25, UnitKind.PC);
      createCellDef(lObj5, null, cellWidth7, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth8 = createMeasure((float)30, UnitKind.PC);
      createCellDef(lObj5, null, cellWidth8, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      Measure cellWidth9 = createMeasure((float)15, UnitKind.PC);
      createCellDef(lObj5, null, cellWidth9, null, ShadingPattern.NO_VALUE, VerticalAlignmentType.TOP);
      {
        Row lObj10 = createRow(lObj5, null, true, "0,0,0", ShadingPattern.NO_VALUE, false, null);
        {
          Cell lObj11 = createCell(lObj10);
          createText(lObj11, "Attributes");
          Cell lObj12 = createCell(lObj10);
          createText(lObj12, "Description");
          Cell lObj13 = createCell(lObj10);
          createText(lObj13, "Possible values (if any)");
          Cell lObj14 = createCell(lObj10);
          createText(lObj14, "Optional");
        }
        Iterator<org.pragmaticmodeling.pxmanual.pxManual.KeywordAttribute> lObj15 = getIterator(element.getAttributes());
        while (lObj15.hasNext()) {
          attributeRow(lObj5, lObj15.next());
          
        }
      }
      createParagraphBreak(parent);
    }
    DocBody _conclusion = element.getConclusion();
    boolean _tripleNotEquals_1 = (_conclusion != null);
    if (_tripleNotEquals_1) {
      toDoc(parent, element.getConclusion(), level);
    }
    List<Usage> usageExamples = this.getUsageExamples(element);
    boolean _isEmpty_2 = usageExamples.isEmpty();
    boolean _not_1 = (!_isEmpty_2);
    if (_not_1) {
      Iterator<org.pragmaticmodeling.pxmanual.pxManual.Usage> lObj16 = getIterator(usageExamples);
      while (lObj16.hasNext()) {
        toUsageExample(parent, lObj16.next());
        
      }
    }
  }
  
  public void tableCell(final ContainerElement parent, final org.pragmaticmodeling.pxmanual.pxManual.Cell tableCell) throws PxDocGenerationException {
    Cell lObj0 = createCell(parent);
    toDoc(lObj0, tableCell.getBody());
  }
  
  public void tableRow(final ContainerElement parent, final org.pragmaticmodeling.pxmanual.pxManual.Row tableRow) throws PxDocGenerationException {
    Row lObj0 = createRow(parent, null, false, null, ShadingPattern.NO_VALUE, false, null);
    Iterator<org.pragmaticmodeling.pxmanual.pxManual.Cell> lObj1 = getIterator(tableRow.getCells());
    while (lObj1.hasNext()) {
      tableCell(lObj0, lObj1.next());
      
    }
  }
  
  public void toDoc(final ContainerElement parent, final ManualElement element, final int level) throws PxDocGenerationException {
    boolean _matched = false;
    if (element instanceof Text) {
      _matched=true;
      toText(parent, ((Text)element));
    }
    if (!_matched) {
      if (element instanceof Page) {
        _matched=true;
        toTitle(parent, ((Page)element), level);
      }
    }
    if (!_matched) {
      if (element instanceof Section) {
        _matched=true;
        toTitle(parent, ((Section)element), level);
      }
    }
    if (!_matched) {
      if (element instanceof Image) {
        _matched=true;
        toImage(parent, ((Image)element));
      }
    }
    if (!_matched) {
      if (element instanceof Keyword) {
        _matched=true;
        toKeyword(parent, ((Keyword)element), level);
      }
    }
    if (!_matched) {
      if (element instanceof Usage) {
        _matched=true;
        toUsageExample(parent, ((Usage)element));
      }
    }
    if (!_matched) {
      if (element instanceof org.pragmaticmodeling.pxmanual.pxManual.Table) {
        _matched=true;
        Table lObj0 = createTable(parent, null, null, createBorder(null, null, 0), null, ShadingPattern.NO_VALUE);
        Iterator<org.pragmaticmodeling.pxmanual.pxManual.Row> lObj1 = getIterator(((org.pragmaticmodeling.pxmanual.pxManual.Table)element).getRows());
        while (lObj1.hasNext()) {
          tableRow(lObj0, lObj1.next());
          
        }
      }
    }
    if (!_matched) {
      if (element instanceof SamePage) {
        _matched=true;
        fr.pragmaticmodeling.pxdoc.SamePage lObj2 = createSamePage(parent);
        toDoc(lObj2, ((SamePage)element).getBody());
      }
    }
  }
  
  public void toDoc(final ContainerElement parent, final ManualElement element) throws PxDocGenerationException {
    toDoc(parent, element, 0);
  }
  
  /**
   * Entry point.
   */
  public void main(final ContainerElement parent, final Document model) throws PxDocGenerationException {
    this.modelDirectory = this.getModelDirectory(model);
    String _file = (new File(getLauncher().getTargetFile())).getAbsolutePath();
    fr.pragmaticmodeling.pxdoc.Document lObj0 = createDocument(parent, _file, "RefDoc", "C:/Users/Public/pxdoc.dotm", createMeasure(getHeaderHeight(), UnitKind.CM), createMeasure(getFooterHeight(), UnitKind.CM));
    addDocument(lObj0);
    {
      createSubDocument(lObj0, "templates/firstpage.docx");
      createPageBreak(lObj0);
      createToc(lObj0, null, "", null);
      createParagraphBreak(lObj0);
      Iterator<org.pragmaticmodeling.pxmanual.pxManual.ManualElement> lObj1 = getIterator(model.getBody().getElements());
      while (lObj1.hasNext()) {
        toDoc(lObj0, lObj1.next(), 1);
        
      }
    }
  }
}
