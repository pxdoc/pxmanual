package org.pragmaticmodeling.pxmanual.pxdoc;

import java.util.HashSet;
import java.util.Set;

import org.pragmaticmodeling.pxdoc.plugins.html2pxdoc.DefaultHtml2PxdocExtension;

import fr.pragmaticmodeling.pxdoc.Font;
import fr.pragmaticmodeling.pxdoc.Hyperlink;
import fr.pragmaticmodeling.pxdoc.Text;
import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;

public class HtmlExtension extends DefaultHtml2PxdocExtension {

	//private ModuleImpl factory = new ModuleImpl();

	private Set<String> keywordWithHyperlinks;

	public HtmlExtension(IPxDocGenerator generator) {
		super(generator);
		keywordWithHyperlinks = new HashSet<String>();
		keywordWithHyperlinks.add("apply");
		keywordWithHyperlinks.add("template");
		keywordWithHyperlinks.add("function");
		keywordWithHyperlinks.add("var");
		keywordWithHyperlinks.add("val");
	}

	@Override
	public void decorate(Font font, String className) {
		if ("pxDocKeyword".equalsIgnoreCase(className)) {
			font.setBold(true);
			font.setColor("0,128,192");
			insertHyperlink(font);
		} else if ("keyword".equalsIgnoreCase(className)) {
			font.setBold(true);
			font.setColor("127,0,85");
			insertHyperlink(font, keywordWithHyperlinks);
		} else if ("pxDocAttributeKeyword".equalsIgnoreCase(className) || "pxDocProperty".equalsIgnoreCase(className)) {
			font.setItalic(true);
			font.setColor("0,128,192");
		} else if ("note".equalsIgnoreCase(className)) {
			font.setBackgroundColor("200,200,250");
		} else if ("citation".equalsIgnoreCase(className)) {
			font.setItalic(true);
			font.setBackgroundColor("200,200,200");
		}
	}

	private void insertHyperlink(Font font) {
		insertHyperlink(font, null);
	}

	private void insertHyperlink(Font font, Set<String> allowed) {
		System.out.println();
		Text text = getText(font);
		if (text != null) {
			String keywordName = getKeywordName(text.getContent());
			if (allowed == null || allowed != null && allowed.contains(keywordName)) {
				Hyperlink hlink = getPxdocFactory().createHyperlink(font, keywordName, null, null);
				hlink.getOwnedElements().add(text);
			}
		}
	}

	private String getKeywordName(String rawName) {
		String name = rawName;
//		if ("#".equals(rawName)) {
//			name = "styleApplication";
//		} else 
		if ("pxDocGenerator".equals(rawName) || "pxDocModule".equals(rawName)) {
			return "pxDocGeneratorOrModule";
		}
		return name;
	}

	private Text getText(Font font) {
		if (font.getOwnedElements().size() == 1 && font.getOwnedElements().get(0) instanceof Text) {
			return (Text) font.getOwnedElements().get(0);
		}
		return null;
	}
}
