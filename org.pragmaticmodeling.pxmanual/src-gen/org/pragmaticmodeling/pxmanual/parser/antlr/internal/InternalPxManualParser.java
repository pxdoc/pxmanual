package org.pragmaticmodeling.pxmanual.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.pragmaticmodeling.pxmanual.services.PxManualGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPxManualParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'document'", "'id:'", "'firstHeadingLevel:'", "'{'", "'}'", "'toc'", "'maxLevel:'", "'list'", "'item'", "'samePage'", "'table'", "'style:'", "'className:'", "'row'", "'cell'", "'span:'", "'width:'", "'vAlign:'", "'image'", "'section'", "'page'", "'example'", "'screenshot:'", "'usage'", "'@'", "'class:'", "'keyword'", "'doc:'", "'prettyTitle:'", "'conclusion:'", "'attribute'", "'optional'", "'possibleValues:'", "'top'", "'middle'", "'bottom'", "'baseline'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalPxManualParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPxManualParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPxManualParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPxManual.g"; }



     	private PxManualGrammarAccess grammarAccess;

        public InternalPxManualParser(TokenStream input, PxManualGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Document";
       	}

       	@Override
       	protected PxManualGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDocument"
    // InternalPxManual.g:65:1: entryRuleDocument returns [EObject current=null] : iv_ruleDocument= ruleDocument EOF ;
    public final EObject entryRuleDocument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocument = null;


        try {
            // InternalPxManual.g:65:49: (iv_ruleDocument= ruleDocument EOF )
            // InternalPxManual.g:66:2: iv_ruleDocument= ruleDocument EOF
            {
             newCompositeNode(grammarAccess.getDocumentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDocument=ruleDocument();

            state._fsp--;

             current =iv_ruleDocument; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // InternalPxManual.g:72:1: ruleDocument returns [EObject current=null] : ( () otherlv_1= 'document' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ( (lv_body_8_0= ruleDocBody ) ) ) ;
    public final EObject ruleDocument() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token lv_id_5_0=null;
        Token otherlv_6=null;
        Token lv_firstHeadingLevel_7_0=null;
        EObject lv_title_3_0 = null;

        EObject lv_body_8_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:78:2: ( ( () otherlv_1= 'document' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ( (lv_body_8_0= ruleDocBody ) ) ) )
            // InternalPxManual.g:79:2: ( () otherlv_1= 'document' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ( (lv_body_8_0= ruleDocBody ) ) )
            {
            // InternalPxManual.g:79:2: ( () otherlv_1= 'document' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ( (lv_body_8_0= ruleDocBody ) ) )
            // InternalPxManual.g:80:3: () otherlv_1= 'document' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ( (lv_body_8_0= ruleDocBody ) )
            {
            // InternalPxManual.g:80:3: ()
            // InternalPxManual.g:81:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDocumentAccess().getDocumentAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getDocumentAccess().getDocumentKeyword_1());
            		
            // InternalPxManual.g:91:3: ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:92:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:92:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:93:5: ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            				
            // InternalPxManual.g:96:5: ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )* )
            // InternalPxManual.g:97:6: ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:97:6: ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) ) )*
            loop1:
            do {
                int alt1=4;
                int LA1_0 = input.LA(1);

                if ( ( LA1_0 == RULE_STRING || LA1_0 >= 35 && LA1_0 <= 36 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
                    alt1=1;
                }
                else if ( LA1_0 == 12 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
                    alt1=2;
                }
                else if ( LA1_0 == 13 && getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
                    alt1=3;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPxManual.g:98:4: ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) )
            	    {
            	    // InternalPxManual.g:98:4: ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) )
            	    // InternalPxManual.g:99:5: {...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleDocument", "getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0)");
            	    }
            	    // InternalPxManual.g:99:105: ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) )
            	    // InternalPxManual.g:100:6: ({...}? => ( (lv_title_3_0= ruleText ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 0);
            	    					
            	    // InternalPxManual.g:103:9: ({...}? => ( (lv_title_3_0= ruleText ) ) )
            	    // InternalPxManual.g:103:10: {...}? => ( (lv_title_3_0= ruleText ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDocument", "true");
            	    }
            	    // InternalPxManual.g:103:19: ( (lv_title_3_0= ruleText ) )
            	    // InternalPxManual.g:103:20: (lv_title_3_0= ruleText )
            	    {
            	    // InternalPxManual.g:103:20: (lv_title_3_0= ruleText )
            	    // InternalPxManual.g:104:10: lv_title_3_0= ruleText
            	    {

            	    										newCompositeNode(grammarAccess.getDocumentAccess().getTitleTextParserRuleCall_2_0_0());
            	    									
            	    pushFollow(FOLLOW_3);
            	    lv_title_3_0=ruleText();

            	    state._fsp--;


            	    										if (current==null) {
            	    											current = createModelElementForParent(grammarAccess.getDocumentRule());
            	    										}
            	    										set(
            	    											current,
            	    											"title",
            	    											lv_title_3_0,
            	    											"org.pragmaticmodeling.pxmanual.PxManual.Text");
            	    										afterParserOrEnumRuleCall();
            	    									

            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:126:4: ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:126:4: ({...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:127:5: {...}? => ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleDocument", "getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1)");
            	    }
            	    // InternalPxManual.g:127:105: ( ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:128:6: ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 1);
            	    					
            	    // InternalPxManual.g:131:9: ({...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:131:10: {...}? => (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDocument", "true");
            	    }
            	    // InternalPxManual.g:131:19: (otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:131:20: otherlv_4= 'id:' ( (lv_id_5_0= RULE_STRING ) )
            	    {
            	    otherlv_4=(Token)match(input,12,FOLLOW_4); 

            	    									newLeafNode(otherlv_4, grammarAccess.getDocumentAccess().getIdKeyword_2_1_0());
            	    								
            	    // InternalPxManual.g:135:9: ( (lv_id_5_0= RULE_STRING ) )
            	    // InternalPxManual.g:136:10: (lv_id_5_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:136:10: (lv_id_5_0= RULE_STRING )
            	    // InternalPxManual.g:137:11: lv_id_5_0= RULE_STRING
            	    {
            	    lv_id_5_0=(Token)match(input,RULE_STRING,FOLLOW_3); 

            	    											newLeafNode(lv_id_5_0, grammarAccess.getDocumentAccess().getIdSTRINGTerminalRuleCall_2_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getDocumentRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"id",
            	    												lv_id_5_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalPxManual.g:159:4: ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:159:4: ({...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) ) )
            	    // InternalPxManual.g:160:5: {...}? => ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleDocument", "getUnorderedGroupHelper().canSelect(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2)");
            	    }
            	    // InternalPxManual.g:160:105: ( ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) ) )
            	    // InternalPxManual.g:161:6: ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getDocumentAccess().getUnorderedGroup_2(), 2);
            	    					
            	    // InternalPxManual.g:164:9: ({...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) ) )
            	    // InternalPxManual.g:164:10: {...}? => (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleDocument", "true");
            	    }
            	    // InternalPxManual.g:164:19: (otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) ) )
            	    // InternalPxManual.g:164:20: otherlv_6= 'firstHeadingLevel:' ( (lv_firstHeadingLevel_7_0= RULE_INT ) )
            	    {
            	    otherlv_6=(Token)match(input,13,FOLLOW_5); 

            	    									newLeafNode(otherlv_6, grammarAccess.getDocumentAccess().getFirstHeadingLevelKeyword_2_2_0());
            	    								
            	    // InternalPxManual.g:168:9: ( (lv_firstHeadingLevel_7_0= RULE_INT ) )
            	    // InternalPxManual.g:169:10: (lv_firstHeadingLevel_7_0= RULE_INT )
            	    {
            	    // InternalPxManual.g:169:10: (lv_firstHeadingLevel_7_0= RULE_INT )
            	    // InternalPxManual.g:170:11: lv_firstHeadingLevel_7_0= RULE_INT
            	    {
            	    lv_firstHeadingLevel_7_0=(Token)match(input,RULE_INT,FOLLOW_3); 

            	    											newLeafNode(lv_firstHeadingLevel_7_0, grammarAccess.getDocumentAccess().getFirstHeadingLevelINTTerminalRuleCall_2_2_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getDocumentRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"firstHeadingLevel",
            	    												lv_firstHeadingLevel_7_0,
            	    												"org.eclipse.xtext.common.Terminals.INT");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getDocumentAccess().getUnorderedGroup_2());
            				

            }

            // InternalPxManual.g:199:3: ( (lv_body_8_0= ruleDocBody ) )
            // InternalPxManual.g:200:4: (lv_body_8_0= ruleDocBody )
            {
            // InternalPxManual.g:200:4: (lv_body_8_0= ruleDocBody )
            // InternalPxManual.g:201:5: lv_body_8_0= ruleDocBody
            {

            					newCompositeNode(grammarAccess.getDocumentAccess().getBodyDocBodyParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_8_0=ruleDocBody();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDocumentRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_8_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleDocBody"
    // InternalPxManual.g:222:1: entryRuleDocBody returns [EObject current=null] : iv_ruleDocBody= ruleDocBody EOF ;
    public final EObject entryRuleDocBody() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocBody = null;


        try {
            // InternalPxManual.g:222:48: (iv_ruleDocBody= ruleDocBody EOF )
            // InternalPxManual.g:223:2: iv_ruleDocBody= ruleDocBody EOF
            {
             newCompositeNode(grammarAccess.getDocBodyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDocBody=ruleDocBody();

            state._fsp--;

             current =iv_ruleDocBody; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocBody"


    // $ANTLR start "ruleDocBody"
    // InternalPxManual.g:229:1: ruleDocBody returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_elements_2_0= ruleManualElement ) )* otherlv_3= '}' ) ;
    public final EObject ruleDocBody() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_elements_2_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:235:2: ( ( () otherlv_1= '{' ( (lv_elements_2_0= ruleManualElement ) )* otherlv_3= '}' ) )
            // InternalPxManual.g:236:2: ( () otherlv_1= '{' ( (lv_elements_2_0= ruleManualElement ) )* otherlv_3= '}' )
            {
            // InternalPxManual.g:236:2: ( () otherlv_1= '{' ( (lv_elements_2_0= ruleManualElement ) )* otherlv_3= '}' )
            // InternalPxManual.g:237:3: () otherlv_1= '{' ( (lv_elements_2_0= ruleManualElement ) )* otherlv_3= '}'
            {
            // InternalPxManual.g:237:3: ()
            // InternalPxManual.g:238:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDocBodyAccess().getDocBodyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_1, grammarAccess.getDocBodyAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalPxManual.g:248:3: ( (lv_elements_2_0= ruleManualElement ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_STRING||LA2_0==16||LA2_0==18||(LA2_0>=20 && LA2_0<=21)||(LA2_0>=29 && LA2_0<=32)||(LA2_0>=34 && LA2_0<=37)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPxManual.g:249:4: (lv_elements_2_0= ruleManualElement )
            	    {
            	    // InternalPxManual.g:249:4: (lv_elements_2_0= ruleManualElement )
            	    // InternalPxManual.g:250:5: lv_elements_2_0= ruleManualElement
            	    {

            	    					newCompositeNode(grammarAccess.getDocBodyAccess().getElementsManualElementParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_elements_2_0=ruleManualElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDocBodyRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_2_0,
            	    						"org.pragmaticmodeling.pxmanual.PxManual.ManualElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_3=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getDocBodyAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocBody"


    // $ANTLR start "entryRuleManualElement"
    // InternalPxManual.g:275:1: entryRuleManualElement returns [EObject current=null] : iv_ruleManualElement= ruleManualElement EOF ;
    public final EObject entryRuleManualElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManualElement = null;


        try {
            // InternalPxManual.g:275:54: (iv_ruleManualElement= ruleManualElement EOF )
            // InternalPxManual.g:276:2: iv_ruleManualElement= ruleManualElement EOF
            {
             newCompositeNode(grammarAccess.getManualElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManualElement=ruleManualElement();

            state._fsp--;

             current =iv_ruleManualElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManualElement"


    // $ANTLR start "ruleManualElement"
    // InternalPxManual.g:282:1: ruleManualElement returns [EObject current=null] : (this_Keyword_0= ruleKeyword | this_AbstractSection_1= ruleAbstractSection | this_Text_2= ruleText | this_Image_3= ruleImage | this_AbstractExample_4= ruleAbstractExample | this_Table_5= ruleTable | this_SamePage_6= ruleSamePage | this_List_7= ruleList | this_Toc_8= ruleToc ) ;
    public final EObject ruleManualElement() throws RecognitionException {
        EObject current = null;

        EObject this_Keyword_0 = null;

        EObject this_AbstractSection_1 = null;

        EObject this_Text_2 = null;

        EObject this_Image_3 = null;

        EObject this_AbstractExample_4 = null;

        EObject this_Table_5 = null;

        EObject this_SamePage_6 = null;

        EObject this_List_7 = null;

        EObject this_Toc_8 = null;



        	enterRule();

        try {
            // InternalPxManual.g:288:2: ( (this_Keyword_0= ruleKeyword | this_AbstractSection_1= ruleAbstractSection | this_Text_2= ruleText | this_Image_3= ruleImage | this_AbstractExample_4= ruleAbstractExample | this_Table_5= ruleTable | this_SamePage_6= ruleSamePage | this_List_7= ruleList | this_Toc_8= ruleToc ) )
            // InternalPxManual.g:289:2: (this_Keyword_0= ruleKeyword | this_AbstractSection_1= ruleAbstractSection | this_Text_2= ruleText | this_Image_3= ruleImage | this_AbstractExample_4= ruleAbstractExample | this_Table_5= ruleTable | this_SamePage_6= ruleSamePage | this_List_7= ruleList | this_Toc_8= ruleToc )
            {
            // InternalPxManual.g:289:2: (this_Keyword_0= ruleKeyword | this_AbstractSection_1= ruleAbstractSection | this_Text_2= ruleText | this_Image_3= ruleImage | this_AbstractExample_4= ruleAbstractExample | this_Table_5= ruleTable | this_SamePage_6= ruleSamePage | this_List_7= ruleList | this_Toc_8= ruleToc )
            int alt3=9;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt3=1;
                }
                break;
            case 30:
            case 31:
                {
                alt3=2;
                }
                break;
            case RULE_STRING:
            case 35:
            case 36:
                {
                alt3=3;
                }
                break;
            case 29:
                {
                alt3=4;
                }
                break;
            case 32:
            case 34:
                {
                alt3=5;
                }
                break;
            case 21:
                {
                alt3=6;
                }
                break;
            case 20:
                {
                alt3=7;
                }
                break;
            case 18:
                {
                alt3=8;
                }
                break;
            case 16:
                {
                alt3=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalPxManual.g:290:3: this_Keyword_0= ruleKeyword
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getKeywordParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Keyword_0=ruleKeyword();

                    state._fsp--;


                    			current = this_Keyword_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPxManual.g:299:3: this_AbstractSection_1= ruleAbstractSection
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getAbstractSectionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AbstractSection_1=ruleAbstractSection();

                    state._fsp--;


                    			current = this_AbstractSection_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPxManual.g:308:3: this_Text_2= ruleText
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getTextParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Text_2=ruleText();

                    state._fsp--;


                    			current = this_Text_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalPxManual.g:317:3: this_Image_3= ruleImage
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getImageParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Image_3=ruleImage();

                    state._fsp--;


                    			current = this_Image_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalPxManual.g:326:3: this_AbstractExample_4= ruleAbstractExample
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getAbstractExampleParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_AbstractExample_4=ruleAbstractExample();

                    state._fsp--;


                    			current = this_AbstractExample_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalPxManual.g:335:3: this_Table_5= ruleTable
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getTableParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Table_5=ruleTable();

                    state._fsp--;


                    			current = this_Table_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalPxManual.g:344:3: this_SamePage_6= ruleSamePage
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getSamePageParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_SamePage_6=ruleSamePage();

                    state._fsp--;


                    			current = this_SamePage_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalPxManual.g:353:3: this_List_7= ruleList
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getListParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_List_7=ruleList();

                    state._fsp--;


                    			current = this_List_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalPxManual.g:362:3: this_Toc_8= ruleToc
                    {

                    			newCompositeNode(grammarAccess.getManualElementAccess().getTocParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_Toc_8=ruleToc();

                    state._fsp--;


                    			current = this_Toc_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManualElement"


    // $ANTLR start "entryRuleToc"
    // InternalPxManual.g:374:1: entryRuleToc returns [EObject current=null] : iv_ruleToc= ruleToc EOF ;
    public final EObject entryRuleToc() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleToc = null;


        try {
            // InternalPxManual.g:374:44: (iv_ruleToc= ruleToc EOF )
            // InternalPxManual.g:375:2: iv_ruleToc= ruleToc EOF
            {
             newCompositeNode(grammarAccess.getTocRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleToc=ruleToc();

            state._fsp--;

             current =iv_ruleToc; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleToc"


    // $ANTLR start "ruleToc"
    // InternalPxManual.g:381:1: ruleToc returns [EObject current=null] : ( () otherlv_1= 'toc' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ) ;
    public final EObject ruleToc() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token lv_maxLevel_5_0=null;
        EObject lv_title_3_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:387:2: ( ( () otherlv_1= 'toc' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ) )
            // InternalPxManual.g:388:2: ( () otherlv_1= 'toc' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) ) ) )
            {
            // InternalPxManual.g:388:2: ( () otherlv_1= 'toc' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) ) ) )
            // InternalPxManual.g:389:3: () otherlv_1= 'toc' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) ) )
            {
            // InternalPxManual.g:389:3: ()
            // InternalPxManual.g:390:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTocAccess().getTocAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getTocAccess().getTocKeyword_1());
            		
            // InternalPxManual.g:400:3: ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:401:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:401:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:402:5: ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getTocAccess().getUnorderedGroup_2());
            				
            // InternalPxManual.g:405:5: ( ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )* )
            // InternalPxManual.g:406:6: ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:406:6: ( ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) ) )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==35) ) {
                    int LA4_2 = input.LA(2);

                    if ( (LA4_2==RULE_ID) ) {
                        int LA4_6 = input.LA(3);

                        if ( (LA4_6==36) ) {
                            int LA4_3 = input.LA(4);

                            if ( (LA4_3==RULE_ID) ) {
                                int LA4_7 = input.LA(5);

                                if ( (LA4_7==RULE_STRING) ) {
                                    int LA4_4 = input.LA(6);

                                    if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                                        alt4=1;
                                    }


                                }


                            }


                        }
                        else if ( (LA4_6==RULE_STRING) ) {
                            int LA4_4 = input.LA(4);

                            if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                                alt4=1;
                            }


                        }


                    }


                }
                else if ( (LA4_0==36) ) {
                    int LA4_3 = input.LA(2);

                    if ( (LA4_3==RULE_ID) ) {
                        int LA4_7 = input.LA(3);

                        if ( (LA4_7==RULE_STRING) ) {
                            int LA4_4 = input.LA(4);

                            if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                                alt4=1;
                            }


                        }


                    }


                }
                else if ( (LA4_0==RULE_STRING) ) {
                    int LA4_4 = input.LA(2);

                    if ( getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
                        alt4=1;
                    }


                }
                else if ( LA4_0 == 17 && getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPxManual.g:407:4: ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) )
            	    {
            	    // InternalPxManual.g:407:4: ({...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) ) )
            	    // InternalPxManual.g:408:5: {...}? => ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleToc", "getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0)");
            	    }
            	    // InternalPxManual.g:408:100: ( ({...}? => ( (lv_title_3_0= ruleText ) ) ) )
            	    // InternalPxManual.g:409:6: ({...}? => ( (lv_title_3_0= ruleText ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getTocAccess().getUnorderedGroup_2(), 0);
            	    					
            	    // InternalPxManual.g:412:9: ({...}? => ( (lv_title_3_0= ruleText ) ) )
            	    // InternalPxManual.g:412:10: {...}? => ( (lv_title_3_0= ruleText ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleToc", "true");
            	    }
            	    // InternalPxManual.g:412:19: ( (lv_title_3_0= ruleText ) )
            	    // InternalPxManual.g:412:20: (lv_title_3_0= ruleText )
            	    {
            	    // InternalPxManual.g:412:20: (lv_title_3_0= ruleText )
            	    // InternalPxManual.g:413:10: lv_title_3_0= ruleText
            	    {

            	    										newCompositeNode(grammarAccess.getTocAccess().getTitleTextParserRuleCall_2_0_0());
            	    									
            	    pushFollow(FOLLOW_7);
            	    lv_title_3_0=ruleText();

            	    state._fsp--;


            	    										if (current==null) {
            	    											current = createModelElementForParent(grammarAccess.getTocRule());
            	    										}
            	    										set(
            	    											current,
            	    											"title",
            	    											lv_title_3_0,
            	    											"org.pragmaticmodeling.pxmanual.PxManual.Text");
            	    										afterParserOrEnumRuleCall();
            	    									

            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTocAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:435:4: ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:435:4: ({...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) ) )
            	    // InternalPxManual.g:436:5: {...}? => ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleToc", "getUnorderedGroupHelper().canSelect(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1)");
            	    }
            	    // InternalPxManual.g:436:100: ( ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) ) )
            	    // InternalPxManual.g:437:6: ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getTocAccess().getUnorderedGroup_2(), 1);
            	    					
            	    // InternalPxManual.g:440:9: ({...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) ) )
            	    // InternalPxManual.g:440:10: {...}? => (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleToc", "true");
            	    }
            	    // InternalPxManual.g:440:19: (otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) ) )
            	    // InternalPxManual.g:440:20: otherlv_4= 'maxLevel:' ( (lv_maxLevel_5_0= RULE_INT ) )
            	    {
            	    otherlv_4=(Token)match(input,17,FOLLOW_5); 

            	    									newLeafNode(otherlv_4, grammarAccess.getTocAccess().getMaxLevelKeyword_2_1_0());
            	    								
            	    // InternalPxManual.g:444:9: ( (lv_maxLevel_5_0= RULE_INT ) )
            	    // InternalPxManual.g:445:10: (lv_maxLevel_5_0= RULE_INT )
            	    {
            	    // InternalPxManual.g:445:10: (lv_maxLevel_5_0= RULE_INT )
            	    // InternalPxManual.g:446:11: lv_maxLevel_5_0= RULE_INT
            	    {
            	    lv_maxLevel_5_0=(Token)match(input,RULE_INT,FOLLOW_7); 

            	    											newLeafNode(lv_maxLevel_5_0, grammarAccess.getTocAccess().getMaxLevelINTTerminalRuleCall_2_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getTocRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"maxLevel",
            	    												lv_maxLevel_5_0,
            	    												"org.eclipse.xtext.common.Terminals.INT");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTocAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getTocAccess().getUnorderedGroup_2());
            				

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleToc"


    // $ANTLR start "entryRuleList"
    // InternalPxManual.g:479:1: entryRuleList returns [EObject current=null] : iv_ruleList= ruleList EOF ;
    public final EObject entryRuleList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleList = null;


        try {
            // InternalPxManual.g:479:45: (iv_ruleList= ruleList EOF )
            // InternalPxManual.g:480:2: iv_ruleList= ruleList EOF
            {
             newCompositeNode(grammarAccess.getListRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleList=ruleList();

            state._fsp--;

             current =iv_ruleList; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleList"


    // $ANTLR start "ruleList"
    // InternalPxManual.g:486:1: ruleList returns [EObject current=null] : ( () otherlv_1= 'list' otherlv_2= '{' ( (lv_items_3_0= ruleListItem ) )* otherlv_4= '}' ) ;
    public final EObject ruleList() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_items_3_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:492:2: ( ( () otherlv_1= 'list' otherlv_2= '{' ( (lv_items_3_0= ruleListItem ) )* otherlv_4= '}' ) )
            // InternalPxManual.g:493:2: ( () otherlv_1= 'list' otherlv_2= '{' ( (lv_items_3_0= ruleListItem ) )* otherlv_4= '}' )
            {
            // InternalPxManual.g:493:2: ( () otherlv_1= 'list' otherlv_2= '{' ( (lv_items_3_0= ruleListItem ) )* otherlv_4= '}' )
            // InternalPxManual.g:494:3: () otherlv_1= 'list' otherlv_2= '{' ( (lv_items_3_0= ruleListItem ) )* otherlv_4= '}'
            {
            // InternalPxManual.g:494:3: ()
            // InternalPxManual.g:495:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getListAccess().getListAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,18,FOLLOW_8); 

            			newLeafNode(otherlv_1, grammarAccess.getListAccess().getListKeyword_1());
            		
            otherlv_2=(Token)match(input,14,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getListAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPxManual.g:509:3: ( (lv_items_3_0= ruleListItem ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==19) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalPxManual.g:510:4: (lv_items_3_0= ruleListItem )
            	    {
            	    // InternalPxManual.g:510:4: (lv_items_3_0= ruleListItem )
            	    // InternalPxManual.g:511:5: lv_items_3_0= ruleListItem
            	    {

            	    					newCompositeNode(grammarAccess.getListAccess().getItemsListItemParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_items_3_0=ruleListItem();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getListRule());
            	    					}
            	    					add(
            	    						current,
            	    						"items",
            	    						lv_items_3_0,
            	    						"org.pragmaticmodeling.pxmanual.PxManual.ListItem");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getListAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleList"


    // $ANTLR start "entryRuleListItem"
    // InternalPxManual.g:536:1: entryRuleListItem returns [EObject current=null] : iv_ruleListItem= ruleListItem EOF ;
    public final EObject entryRuleListItem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleListItem = null;


        try {
            // InternalPxManual.g:536:49: (iv_ruleListItem= ruleListItem EOF )
            // InternalPxManual.g:537:2: iv_ruleListItem= ruleListItem EOF
            {
             newCompositeNode(grammarAccess.getListItemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleListItem=ruleListItem();

            state._fsp--;

             current =iv_ruleListItem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleListItem"


    // $ANTLR start "ruleListItem"
    // InternalPxManual.g:543:1: ruleListItem returns [EObject current=null] : (otherlv_0= 'item' ( (lv_body_1_0= ruleDocBody ) ) ) ;
    public final EObject ruleListItem() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_body_1_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:549:2: ( (otherlv_0= 'item' ( (lv_body_1_0= ruleDocBody ) ) ) )
            // InternalPxManual.g:550:2: (otherlv_0= 'item' ( (lv_body_1_0= ruleDocBody ) ) )
            {
            // InternalPxManual.g:550:2: (otherlv_0= 'item' ( (lv_body_1_0= ruleDocBody ) ) )
            // InternalPxManual.g:551:3: otherlv_0= 'item' ( (lv_body_1_0= ruleDocBody ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getListItemAccess().getItemKeyword_0());
            		
            // InternalPxManual.g:555:3: ( (lv_body_1_0= ruleDocBody ) )
            // InternalPxManual.g:556:4: (lv_body_1_0= ruleDocBody )
            {
            // InternalPxManual.g:556:4: (lv_body_1_0= ruleDocBody )
            // InternalPxManual.g:557:5: lv_body_1_0= ruleDocBody
            {

            					newCompositeNode(grammarAccess.getListItemAccess().getBodyDocBodyParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_1_0=ruleDocBody();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getListItemRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_1_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleListItem"


    // $ANTLR start "entryRuleSamePage"
    // InternalPxManual.g:578:1: entryRuleSamePage returns [EObject current=null] : iv_ruleSamePage= ruleSamePage EOF ;
    public final EObject entryRuleSamePage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSamePage = null;


        try {
            // InternalPxManual.g:578:49: (iv_ruleSamePage= ruleSamePage EOF )
            // InternalPxManual.g:579:2: iv_ruleSamePage= ruleSamePage EOF
            {
             newCompositeNode(grammarAccess.getSamePageRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSamePage=ruleSamePage();

            state._fsp--;

             current =iv_ruleSamePage; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSamePage"


    // $ANTLR start "ruleSamePage"
    // InternalPxManual.g:585:1: ruleSamePage returns [EObject current=null] : (otherlv_0= 'samePage' ( (lv_body_1_0= ruleDocBody ) ) ) ;
    public final EObject ruleSamePage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_body_1_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:591:2: ( (otherlv_0= 'samePage' ( (lv_body_1_0= ruleDocBody ) ) ) )
            // InternalPxManual.g:592:2: (otherlv_0= 'samePage' ( (lv_body_1_0= ruleDocBody ) ) )
            {
            // InternalPxManual.g:592:2: (otherlv_0= 'samePage' ( (lv_body_1_0= ruleDocBody ) ) )
            // InternalPxManual.g:593:3: otherlv_0= 'samePage' ( (lv_body_1_0= ruleDocBody ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSamePageAccess().getSamePageKeyword_0());
            		
            // InternalPxManual.g:597:3: ( (lv_body_1_0= ruleDocBody ) )
            // InternalPxManual.g:598:4: (lv_body_1_0= ruleDocBody )
            {
            // InternalPxManual.g:598:4: (lv_body_1_0= ruleDocBody )
            // InternalPxManual.g:599:5: lv_body_1_0= ruleDocBody
            {

            					newCompositeNode(grammarAccess.getSamePageAccess().getBodyDocBodyParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_1_0=ruleDocBody();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSamePageRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_1_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSamePage"


    // $ANTLR start "entryRuleTable"
    // InternalPxManual.g:620:1: entryRuleTable returns [EObject current=null] : iv_ruleTable= ruleTable EOF ;
    public final EObject entryRuleTable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTable = null;


        try {
            // InternalPxManual.g:620:46: (iv_ruleTable= ruleTable EOF )
            // InternalPxManual.g:621:2: iv_ruleTable= ruleTable EOF
            {
             newCompositeNode(grammarAccess.getTableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTable=ruleTable();

            state._fsp--;

             current =iv_ruleTable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTable"


    // $ANTLR start "ruleTable"
    // InternalPxManual.g:627:1: ruleTable returns [EObject current=null] : ( () otherlv_1= 'table' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_rows_8_0= ruleRow ) )* otherlv_9= '}' ) ;
    public final EObject ruleTable() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_style_4_0=null;
        Token otherlv_5=null;
        Token lv_className_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_rows_8_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:633:2: ( ( () otherlv_1= 'table' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_rows_8_0= ruleRow ) )* otherlv_9= '}' ) )
            // InternalPxManual.g:634:2: ( () otherlv_1= 'table' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_rows_8_0= ruleRow ) )* otherlv_9= '}' )
            {
            // InternalPxManual.g:634:2: ( () otherlv_1= 'table' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_rows_8_0= ruleRow ) )* otherlv_9= '}' )
            // InternalPxManual.g:635:3: () otherlv_1= 'table' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_rows_8_0= ruleRow ) )* otherlv_9= '}'
            {
            // InternalPxManual.g:635:3: ()
            // InternalPxManual.g:636:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTableAccess().getTableAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,21,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getTableAccess().getTableKeyword_1());
            		
            // InternalPxManual.g:646:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:647:4: ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:647:4: ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:648:5: ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getTableAccess().getUnorderedGroup_2());
            				
            // InternalPxManual.g:651:5: ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* )
            // InternalPxManual.g:652:6: ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:652:6: ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )*
            loop6:
            do {
                int alt6=3;
                int LA6_0 = input.LA(1);

                if ( LA6_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0) ) {
                    alt6=1;
                }
                else if ( LA6_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1) ) {
                    alt6=2;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalPxManual.g:653:4: ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:653:4: ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:654:5: {...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleTable", "getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0)");
            	    }
            	    // InternalPxManual.g:654:102: ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:655:6: ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getTableAccess().getUnorderedGroup_2(), 0);
            	    					
            	    // InternalPxManual.g:658:9: ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:658:10: {...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleTable", "true");
            	    }
            	    // InternalPxManual.g:658:19: (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:658:20: otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) )
            	    {
            	    otherlv_3=(Token)match(input,22,FOLLOW_4); 

            	    									newLeafNode(otherlv_3, grammarAccess.getTableAccess().getStyleKeyword_2_0_0());
            	    								
            	    // InternalPxManual.g:662:9: ( (lv_style_4_0= RULE_STRING ) )
            	    // InternalPxManual.g:663:10: (lv_style_4_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:663:10: (lv_style_4_0= RULE_STRING )
            	    // InternalPxManual.g:664:11: lv_style_4_0= RULE_STRING
            	    {
            	    lv_style_4_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            	    											newLeafNode(lv_style_4_0, grammarAccess.getTableAccess().getStyleSTRINGTerminalRuleCall_2_0_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getTableRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"style",
            	    												lv_style_4_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTableAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:686:4: ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:686:4: ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:687:5: {...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleTable", "getUnorderedGroupHelper().canSelect(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1)");
            	    }
            	    // InternalPxManual.g:687:102: ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:688:6: ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getTableAccess().getUnorderedGroup_2(), 1);
            	    					
            	    // InternalPxManual.g:691:9: ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:691:10: {...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleTable", "true");
            	    }
            	    // InternalPxManual.g:691:19: (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:691:20: otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) )
            	    {
            	    otherlv_5=(Token)match(input,23,FOLLOW_4); 

            	    									newLeafNode(otherlv_5, grammarAccess.getTableAccess().getClassNameKeyword_2_1_0());
            	    								
            	    // InternalPxManual.g:695:9: ( (lv_className_6_0= RULE_STRING ) )
            	    // InternalPxManual.g:696:10: (lv_className_6_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:696:10: (lv_className_6_0= RULE_STRING )
            	    // InternalPxManual.g:697:11: lv_className_6_0= RULE_STRING
            	    {
            	    lv_className_6_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            	    											newLeafNode(lv_className_6_0, grammarAccess.getTableAccess().getClassNameSTRINGTerminalRuleCall_2_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getTableRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"className",
            	    												lv_className_6_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getTableAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getTableAccess().getUnorderedGroup_2());
            				

            }

            otherlv_7=(Token)match(input,14,FOLLOW_11); 

            			newLeafNode(otherlv_7, grammarAccess.getTableAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPxManual.g:730:3: ( (lv_rows_8_0= ruleRow ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==24) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalPxManual.g:731:4: (lv_rows_8_0= ruleRow )
            	    {
            	    // InternalPxManual.g:731:4: (lv_rows_8_0= ruleRow )
            	    // InternalPxManual.g:732:5: lv_rows_8_0= ruleRow
            	    {

            	    					newCompositeNode(grammarAccess.getTableAccess().getRowsRowParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_11);
            	    lv_rows_8_0=ruleRow();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getTableRule());
            	    					}
            	    					add(
            	    						current,
            	    						"rows",
            	    						lv_rows_8_0,
            	    						"org.pragmaticmodeling.pxmanual.PxManual.Row");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_9=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getTableAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTable"


    // $ANTLR start "entryRuleRow"
    // InternalPxManual.g:757:1: entryRuleRow returns [EObject current=null] : iv_ruleRow= ruleRow EOF ;
    public final EObject entryRuleRow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRow = null;


        try {
            // InternalPxManual.g:757:44: (iv_ruleRow= ruleRow EOF )
            // InternalPxManual.g:758:2: iv_ruleRow= ruleRow EOF
            {
             newCompositeNode(grammarAccess.getRowRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRow=ruleRow();

            state._fsp--;

             current =iv_ruleRow; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRow"


    // $ANTLR start "ruleRow"
    // InternalPxManual.g:764:1: ruleRow returns [EObject current=null] : ( () otherlv_1= 'row' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_cells_8_0= ruleCell ) )* otherlv_9= '}' ) ;
    public final EObject ruleRow() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_style_4_0=null;
        Token otherlv_5=null;
        Token lv_className_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_cells_8_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:770:2: ( ( () otherlv_1= 'row' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_cells_8_0= ruleCell ) )* otherlv_9= '}' ) )
            // InternalPxManual.g:771:2: ( () otherlv_1= 'row' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_cells_8_0= ruleCell ) )* otherlv_9= '}' )
            {
            // InternalPxManual.g:771:2: ( () otherlv_1= 'row' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_cells_8_0= ruleCell ) )* otherlv_9= '}' )
            // InternalPxManual.g:772:3: () otherlv_1= 'row' ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) otherlv_7= '{' ( (lv_cells_8_0= ruleCell ) )* otherlv_9= '}'
            {
            // InternalPxManual.g:772:3: ()
            // InternalPxManual.g:773:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRowAccess().getRowAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,24,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getRowAccess().getRowKeyword_1());
            		
            // InternalPxManual.g:783:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:784:4: ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:784:4: ( ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:785:5: ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getRowAccess().getUnorderedGroup_2());
            				
            // InternalPxManual.g:788:5: ( ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )* )
            // InternalPxManual.g:789:6: ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:789:6: ( ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) ) )*
            loop8:
            do {
                int alt8=3;
                int LA8_0 = input.LA(1);

                if ( LA8_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0) ) {
                    alt8=1;
                }
                else if ( LA8_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1) ) {
                    alt8=2;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalPxManual.g:790:4: ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:790:4: ({...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:791:5: {...}? => ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleRow", "getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0)");
            	    }
            	    // InternalPxManual.g:791:100: ( ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:792:6: ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getRowAccess().getUnorderedGroup_2(), 0);
            	    					
            	    // InternalPxManual.g:795:9: ({...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:795:10: {...}? => (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleRow", "true");
            	    }
            	    // InternalPxManual.g:795:19: (otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:795:20: otherlv_3= 'style:' ( (lv_style_4_0= RULE_STRING ) )
            	    {
            	    otherlv_3=(Token)match(input,22,FOLLOW_4); 

            	    									newLeafNode(otherlv_3, grammarAccess.getRowAccess().getStyleKeyword_2_0_0());
            	    								
            	    // InternalPxManual.g:799:9: ( (lv_style_4_0= RULE_STRING ) )
            	    // InternalPxManual.g:800:10: (lv_style_4_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:800:10: (lv_style_4_0= RULE_STRING )
            	    // InternalPxManual.g:801:11: lv_style_4_0= RULE_STRING
            	    {
            	    lv_style_4_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            	    											newLeafNode(lv_style_4_0, grammarAccess.getRowAccess().getStyleSTRINGTerminalRuleCall_2_0_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getRowRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"style",
            	    												lv_style_4_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getRowAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:823:4: ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:823:4: ({...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:824:5: {...}? => ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleRow", "getUnorderedGroupHelper().canSelect(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1)");
            	    }
            	    // InternalPxManual.g:824:100: ( ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:825:6: ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getRowAccess().getUnorderedGroup_2(), 1);
            	    					
            	    // InternalPxManual.g:828:9: ({...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:828:10: {...}? => (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleRow", "true");
            	    }
            	    // InternalPxManual.g:828:19: (otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:828:20: otherlv_5= 'className:' ( (lv_className_6_0= RULE_STRING ) )
            	    {
            	    otherlv_5=(Token)match(input,23,FOLLOW_4); 

            	    									newLeafNode(otherlv_5, grammarAccess.getRowAccess().getClassNameKeyword_2_1_0());
            	    								
            	    // InternalPxManual.g:832:9: ( (lv_className_6_0= RULE_STRING ) )
            	    // InternalPxManual.g:833:10: (lv_className_6_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:833:10: (lv_className_6_0= RULE_STRING )
            	    // InternalPxManual.g:834:11: lv_className_6_0= RULE_STRING
            	    {
            	    lv_className_6_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

            	    											newLeafNode(lv_className_6_0, grammarAccess.getRowAccess().getClassNameSTRINGTerminalRuleCall_2_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getRowRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"className",
            	    												lv_className_6_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getRowAccess().getUnorderedGroup_2());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getRowAccess().getUnorderedGroup_2());
            				

            }

            otherlv_7=(Token)match(input,14,FOLLOW_12); 

            			newLeafNode(otherlv_7, grammarAccess.getRowAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPxManual.g:867:3: ( (lv_cells_8_0= ruleCell ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==25) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalPxManual.g:868:4: (lv_cells_8_0= ruleCell )
            	    {
            	    // InternalPxManual.g:868:4: (lv_cells_8_0= ruleCell )
            	    // InternalPxManual.g:869:5: lv_cells_8_0= ruleCell
            	    {

            	    					newCompositeNode(grammarAccess.getRowAccess().getCellsCellParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_cells_8_0=ruleCell();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getRowRule());
            	    					}
            	    					add(
            	    						current,
            	    						"cells",
            	    						lv_cells_8_0,
            	    						"org.pragmaticmodeling.pxmanual.PxManual.Cell");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_9=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getRowAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRow"


    // $ANTLR start "entryRuleCell"
    // InternalPxManual.g:894:1: entryRuleCell returns [EObject current=null] : iv_ruleCell= ruleCell EOF ;
    public final EObject entryRuleCell() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCell = null;


        try {
            // InternalPxManual.g:894:45: (iv_ruleCell= ruleCell EOF )
            // InternalPxManual.g:895:2: iv_ruleCell= ruleCell EOF
            {
             newCompositeNode(grammarAccess.getCellRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCell=ruleCell();

            state._fsp--;

             current =iv_ruleCell; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCell"


    // $ANTLR start "ruleCell"
    // InternalPxManual.g:901:1: ruleCell returns [EObject current=null] : (otherlv_0= 'cell' ( ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) ) ) ( (lv_body_12_0= ruleDocBody ) ) ) ;
    public final EObject ruleCell() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_style_3_0=null;
        Token otherlv_4=null;
        Token lv_className_5_0=null;
        Token otherlv_6=null;
        Token lv_span_7_0=null;
        Token otherlv_8=null;
        Token lv_width_9_0=null;
        Token otherlv_10=null;
        Enumerator lv_vAlign_11_0 = null;

        EObject lv_body_12_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:907:2: ( (otherlv_0= 'cell' ( ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) ) ) ( (lv_body_12_0= ruleDocBody ) ) ) )
            // InternalPxManual.g:908:2: (otherlv_0= 'cell' ( ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) ) ) ( (lv_body_12_0= ruleDocBody ) ) )
            {
            // InternalPxManual.g:908:2: (otherlv_0= 'cell' ( ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) ) ) ( (lv_body_12_0= ruleDocBody ) ) )
            // InternalPxManual.g:909:3: otherlv_0= 'cell' ( ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) ) ) ( (lv_body_12_0= ruleDocBody ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getCellAccess().getCellKeyword_0());
            		
            // InternalPxManual.g:913:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:914:4: ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:914:4: ( ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:915:5: ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getCellAccess().getUnorderedGroup_1());
            				
            // InternalPxManual.g:918:5: ( ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )* )
            // InternalPxManual.g:919:6: ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:919:6: ( ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) ) )*
            loop10:
            do {
                int alt10=6;
                int LA10_0 = input.LA(1);

                if ( LA10_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
                    alt10=1;
                }
                else if ( LA10_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
                    alt10=2;
                }
                else if ( LA10_0 == 26 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
                    alt10=3;
                }
                else if ( LA10_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
                    alt10=4;
                }
                else if ( LA10_0 == 28 && getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
                    alt10=5;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalPxManual.g:920:4: ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:920:4: ({...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:921:5: {...}? => ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0)");
            	    }
            	    // InternalPxManual.g:921:101: ( ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:922:6: ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 0);
            	    					
            	    // InternalPxManual.g:925:9: ({...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:925:10: {...}? => (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "true");
            	    }
            	    // InternalPxManual.g:925:19: (otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:925:20: otherlv_2= 'style:' ( (lv_style_3_0= RULE_STRING ) )
            	    {
            	    otherlv_2=(Token)match(input,22,FOLLOW_4); 

            	    									newLeafNode(otherlv_2, grammarAccess.getCellAccess().getStyleKeyword_1_0_0());
            	    								
            	    // InternalPxManual.g:929:9: ( (lv_style_3_0= RULE_STRING ) )
            	    // InternalPxManual.g:930:10: (lv_style_3_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:930:10: (lv_style_3_0= RULE_STRING )
            	    // InternalPxManual.g:931:11: lv_style_3_0= RULE_STRING
            	    {
            	    lv_style_3_0=(Token)match(input,RULE_STRING,FOLLOW_13); 

            	    											newLeafNode(lv_style_3_0, grammarAccess.getCellAccess().getStyleSTRINGTerminalRuleCall_1_0_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getCellRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"style",
            	    												lv_style_3_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:953:4: ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:953:4: ({...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:954:5: {...}? => ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1)");
            	    }
            	    // InternalPxManual.g:954:101: ( ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:955:6: ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 1);
            	    					
            	    // InternalPxManual.g:958:9: ({...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:958:10: {...}? => (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "true");
            	    }
            	    // InternalPxManual.g:958:19: (otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:958:20: otherlv_4= 'className:' ( (lv_className_5_0= RULE_STRING ) )
            	    {
            	    otherlv_4=(Token)match(input,23,FOLLOW_4); 

            	    									newLeafNode(otherlv_4, grammarAccess.getCellAccess().getClassNameKeyword_1_1_0());
            	    								
            	    // InternalPxManual.g:962:9: ( (lv_className_5_0= RULE_STRING ) )
            	    // InternalPxManual.g:963:10: (lv_className_5_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:963:10: (lv_className_5_0= RULE_STRING )
            	    // InternalPxManual.g:964:11: lv_className_5_0= RULE_STRING
            	    {
            	    lv_className_5_0=(Token)match(input,RULE_STRING,FOLLOW_13); 

            	    											newLeafNode(lv_className_5_0, grammarAccess.getCellAccess().getClassNameSTRINGTerminalRuleCall_1_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getCellRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"className",
            	    												lv_className_5_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalPxManual.g:986:4: ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:986:4: ({...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) ) )
            	    // InternalPxManual.g:987:5: {...}? => ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2)");
            	    }
            	    // InternalPxManual.g:987:101: ( ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) ) )
            	    // InternalPxManual.g:988:6: ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 2);
            	    					
            	    // InternalPxManual.g:991:9: ({...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) ) )
            	    // InternalPxManual.g:991:10: {...}? => (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "true");
            	    }
            	    // InternalPxManual.g:991:19: (otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) ) )
            	    // InternalPxManual.g:991:20: otherlv_6= 'span:' ( (lv_span_7_0= RULE_INT ) )
            	    {
            	    otherlv_6=(Token)match(input,26,FOLLOW_5); 

            	    									newLeafNode(otherlv_6, grammarAccess.getCellAccess().getSpanKeyword_1_2_0());
            	    								
            	    // InternalPxManual.g:995:9: ( (lv_span_7_0= RULE_INT ) )
            	    // InternalPxManual.g:996:10: (lv_span_7_0= RULE_INT )
            	    {
            	    // InternalPxManual.g:996:10: (lv_span_7_0= RULE_INT )
            	    // InternalPxManual.g:997:11: lv_span_7_0= RULE_INT
            	    {
            	    lv_span_7_0=(Token)match(input,RULE_INT,FOLLOW_13); 

            	    											newLeafNode(lv_span_7_0, grammarAccess.getCellAccess().getSpanINTTerminalRuleCall_1_2_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getCellRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"span",
            	    												lv_span_7_0,
            	    												"org.eclipse.xtext.common.Terminals.INT");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalPxManual.g:1019:4: ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1019:4: ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:1020:5: {...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3)");
            	    }
            	    // InternalPxManual.g:1020:101: ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:1021:6: ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 3);
            	    					
            	    // InternalPxManual.g:1024:9: ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:1024:10: {...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "true");
            	    }
            	    // InternalPxManual.g:1024:19: (otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:1024:20: otherlv_8= 'width:' ( (lv_width_9_0= RULE_STRING ) )
            	    {
            	    otherlv_8=(Token)match(input,27,FOLLOW_4); 

            	    									newLeafNode(otherlv_8, grammarAccess.getCellAccess().getWidthKeyword_1_3_0());
            	    								
            	    // InternalPxManual.g:1028:9: ( (lv_width_9_0= RULE_STRING ) )
            	    // InternalPxManual.g:1029:10: (lv_width_9_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:1029:10: (lv_width_9_0= RULE_STRING )
            	    // InternalPxManual.g:1030:11: lv_width_9_0= RULE_STRING
            	    {
            	    lv_width_9_0=(Token)match(input,RULE_STRING,FOLLOW_13); 

            	    											newLeafNode(lv_width_9_0, grammarAccess.getCellAccess().getWidthSTRINGTerminalRuleCall_1_3_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getCellRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"width",
            	    												lv_width_9_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalPxManual.g:1052:4: ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1052:4: ({...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) ) )
            	    // InternalPxManual.g:1053:5: {...}? => ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "getUnorderedGroupHelper().canSelect(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4)");
            	    }
            	    // InternalPxManual.g:1053:101: ( ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) ) )
            	    // InternalPxManual.g:1054:6: ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getCellAccess().getUnorderedGroup_1(), 4);
            	    					
            	    // InternalPxManual.g:1057:9: ({...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) ) )
            	    // InternalPxManual.g:1057:10: {...}? => (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleCell", "true");
            	    }
            	    // InternalPxManual.g:1057:19: (otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) ) )
            	    // InternalPxManual.g:1057:20: otherlv_10= 'vAlign:' ( (lv_vAlign_11_0= ruleVerticalAlignment ) )
            	    {
            	    otherlv_10=(Token)match(input,28,FOLLOW_14); 

            	    									newLeafNode(otherlv_10, grammarAccess.getCellAccess().getVAlignKeyword_1_4_0());
            	    								
            	    // InternalPxManual.g:1061:9: ( (lv_vAlign_11_0= ruleVerticalAlignment ) )
            	    // InternalPxManual.g:1062:10: (lv_vAlign_11_0= ruleVerticalAlignment )
            	    {
            	    // InternalPxManual.g:1062:10: (lv_vAlign_11_0= ruleVerticalAlignment )
            	    // InternalPxManual.g:1063:11: lv_vAlign_11_0= ruleVerticalAlignment
            	    {

            	    											newCompositeNode(grammarAccess.getCellAccess().getVAlignVerticalAlignmentEnumRuleCall_1_4_1_0());
            	    										
            	    pushFollow(FOLLOW_13);
            	    lv_vAlign_11_0=ruleVerticalAlignment();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getCellRule());
            	    											}
            	    											set(
            	    												current,
            	    												"vAlign",
            	    												lv_vAlign_11_0,
            	    												"org.pragmaticmodeling.pxmanual.PxManual.VerticalAlignment");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getCellAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getCellAccess().getUnorderedGroup_1());
            				

            }

            // InternalPxManual.g:1093:3: ( (lv_body_12_0= ruleDocBody ) )
            // InternalPxManual.g:1094:4: (lv_body_12_0= ruleDocBody )
            {
            // InternalPxManual.g:1094:4: (lv_body_12_0= ruleDocBody )
            // InternalPxManual.g:1095:5: lv_body_12_0= ruleDocBody
            {

            					newCompositeNode(grammarAccess.getCellAccess().getBodyDocBodyParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_12_0=ruleDocBody();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCellRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_12_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCell"


    // $ANTLR start "entryRuleImage"
    // InternalPxManual.g:1116:1: entryRuleImage returns [EObject current=null] : iv_ruleImage= ruleImage EOF ;
    public final EObject entryRuleImage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImage = null;


        try {
            // InternalPxManual.g:1116:46: (iv_ruleImage= ruleImage EOF )
            // InternalPxManual.g:1117:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // InternalPxManual.g:1123:1: ruleImage returns [EObject current=null] : (otherlv_0= 'image' ( (lv_name_1_0= RULE_ID ) )? ( (lv_path_2_0= RULE_STRING ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ) ;
    public final EObject ruleImage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_path_2_0=null;
        Token otherlv_4=null;
        Token lv_style_5_0=null;
        Token otherlv_6=null;
        Token lv_className_7_0=null;
        Token otherlv_8=null;
        Token lv_width_9_0=null;


        	enterRule();

        try {
            // InternalPxManual.g:1129:2: ( (otherlv_0= 'image' ( (lv_name_1_0= RULE_ID ) )? ( (lv_path_2_0= RULE_STRING ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) ) ) ) )
            // InternalPxManual.g:1130:2: (otherlv_0= 'image' ( (lv_name_1_0= RULE_ID ) )? ( (lv_path_2_0= RULE_STRING ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) ) ) )
            {
            // InternalPxManual.g:1130:2: (otherlv_0= 'image' ( (lv_name_1_0= RULE_ID ) )? ( (lv_path_2_0= RULE_STRING ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) ) ) )
            // InternalPxManual.g:1131:3: otherlv_0= 'image' ( (lv_name_1_0= RULE_ID ) )? ( (lv_path_2_0= RULE_STRING ) ) ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getImageAccess().getImageKeyword_0());
            		
            // InternalPxManual.g:1135:3: ( (lv_name_1_0= RULE_ID ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_ID) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalPxManual.g:1136:4: (lv_name_1_0= RULE_ID )
                    {
                    // InternalPxManual.g:1136:4: (lv_name_1_0= RULE_ID )
                    // InternalPxManual.g:1137:5: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

                    					newLeafNode(lv_name_1_0, grammarAccess.getImageAccess().getNameIDTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getImageRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_1_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }
                    break;

            }

            // InternalPxManual.g:1153:3: ( (lv_path_2_0= RULE_STRING ) )
            // InternalPxManual.g:1154:4: (lv_path_2_0= RULE_STRING )
            {
            // InternalPxManual.g:1154:4: (lv_path_2_0= RULE_STRING )
            // InternalPxManual.g:1155:5: lv_path_2_0= RULE_STRING
            {
            lv_path_2_0=(Token)match(input,RULE_STRING,FOLLOW_16); 

            					newLeafNode(lv_path_2_0, grammarAccess.getImageAccess().getPathSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImageRule());
            					}
            					setWithLastConsumed(
            						current,
            						"path",
            						lv_path_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalPxManual.g:1171:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:1172:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:1172:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:1173:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getImageAccess().getUnorderedGroup_3());
            				
            // InternalPxManual.g:1176:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )* )
            // InternalPxManual.g:1177:6: ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:1177:6: ( ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) ) )*
            loop12:
            do {
                int alt12=4;
                int LA12_0 = input.LA(1);

                if ( LA12_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
                    alt12=1;
                }
                else if ( LA12_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
                    alt12=2;
                }
                else if ( LA12_0 == 27 && getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
                    alt12=3;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalPxManual.g:1178:4: ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1178:4: ({...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:1179:5: {...}? => ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalPxManual.g:1179:102: ( ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:1180:6: ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 0);
            	    					
            	    // InternalPxManual.g:1183:9: ({...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:1183:10: {...}? => (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "true");
            	    }
            	    // InternalPxManual.g:1183:19: (otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:1183:20: otherlv_4= 'style:' ( (lv_style_5_0= RULE_STRING ) )
            	    {
            	    otherlv_4=(Token)match(input,22,FOLLOW_4); 

            	    									newLeafNode(otherlv_4, grammarAccess.getImageAccess().getStyleKeyword_3_0_0());
            	    								
            	    // InternalPxManual.g:1187:9: ( (lv_style_5_0= RULE_STRING ) )
            	    // InternalPxManual.g:1188:10: (lv_style_5_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:1188:10: (lv_style_5_0= RULE_STRING )
            	    // InternalPxManual.g:1189:11: lv_style_5_0= RULE_STRING
            	    {
            	    lv_style_5_0=(Token)match(input,RULE_STRING,FOLLOW_16); 

            	    											newLeafNode(lv_style_5_0, grammarAccess.getImageAccess().getStyleSTRINGTerminalRuleCall_3_0_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getImageRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"style",
            	    												lv_style_5_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:1211:4: ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1211:4: ({...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:1212:5: {...}? => ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalPxManual.g:1212:102: ( ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:1213:6: ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 1);
            	    					
            	    // InternalPxManual.g:1216:9: ({...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:1216:10: {...}? => (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "true");
            	    }
            	    // InternalPxManual.g:1216:19: (otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:1216:20: otherlv_6= 'className:' ( (lv_className_7_0= RULE_STRING ) )
            	    {
            	    otherlv_6=(Token)match(input,23,FOLLOW_4); 

            	    									newLeafNode(otherlv_6, grammarAccess.getImageAccess().getClassNameKeyword_3_1_0());
            	    								
            	    // InternalPxManual.g:1220:9: ( (lv_className_7_0= RULE_STRING ) )
            	    // InternalPxManual.g:1221:10: (lv_className_7_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:1221:10: (lv_className_7_0= RULE_STRING )
            	    // InternalPxManual.g:1222:11: lv_className_7_0= RULE_STRING
            	    {
            	    lv_className_7_0=(Token)match(input,RULE_STRING,FOLLOW_16); 

            	    											newLeafNode(lv_className_7_0, grammarAccess.getImageAccess().getClassNameSTRINGTerminalRuleCall_3_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getImageRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"className",
            	    												lv_className_7_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalPxManual.g:1244:4: ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1244:4: ({...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) ) )
            	    // InternalPxManual.g:1245:5: {...}? => ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "getUnorderedGroupHelper().canSelect(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalPxManual.g:1245:102: ( ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) ) )
            	    // InternalPxManual.g:1246:6: ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getImageAccess().getUnorderedGroup_3(), 2);
            	    					
            	    // InternalPxManual.g:1249:9: ({...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) ) )
            	    // InternalPxManual.g:1249:10: {...}? => (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleImage", "true");
            	    }
            	    // InternalPxManual.g:1249:19: (otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) ) )
            	    // InternalPxManual.g:1249:20: otherlv_8= 'width:' ( (lv_width_9_0= RULE_INT ) )
            	    {
            	    otherlv_8=(Token)match(input,27,FOLLOW_5); 

            	    									newLeafNode(otherlv_8, grammarAccess.getImageAccess().getWidthKeyword_3_2_0());
            	    								
            	    // InternalPxManual.g:1253:9: ( (lv_width_9_0= RULE_INT ) )
            	    // InternalPxManual.g:1254:10: (lv_width_9_0= RULE_INT )
            	    {
            	    // InternalPxManual.g:1254:10: (lv_width_9_0= RULE_INT )
            	    // InternalPxManual.g:1255:11: lv_width_9_0= RULE_INT
            	    {
            	    lv_width_9_0=(Token)match(input,RULE_INT,FOLLOW_16); 

            	    											newLeafNode(lv_width_9_0, grammarAccess.getImageAccess().getWidthINTTerminalRuleCall_3_2_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getImageRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"width",
            	    												lv_width_9_0,
            	    												"org.eclipse.xtext.common.Terminals.INT");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getImageAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getImageAccess().getUnorderedGroup_3());
            				

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleAbstractSection"
    // InternalPxManual.g:1288:1: entryRuleAbstractSection returns [EObject current=null] : iv_ruleAbstractSection= ruleAbstractSection EOF ;
    public final EObject entryRuleAbstractSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractSection = null;


        try {
            // InternalPxManual.g:1288:56: (iv_ruleAbstractSection= ruleAbstractSection EOF )
            // InternalPxManual.g:1289:2: iv_ruleAbstractSection= ruleAbstractSection EOF
            {
             newCompositeNode(grammarAccess.getAbstractSectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractSection=ruleAbstractSection();

            state._fsp--;

             current =iv_ruleAbstractSection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractSection"


    // $ANTLR start "ruleAbstractSection"
    // InternalPxManual.g:1295:1: ruleAbstractSection returns [EObject current=null] : (this_Section_0= ruleSection | this_Page_1= rulePage ) ;
    public final EObject ruleAbstractSection() throws RecognitionException {
        EObject current = null;

        EObject this_Section_0 = null;

        EObject this_Page_1 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1301:2: ( (this_Section_0= ruleSection | this_Page_1= rulePage ) )
            // InternalPxManual.g:1302:2: (this_Section_0= ruleSection | this_Page_1= rulePage )
            {
            // InternalPxManual.g:1302:2: (this_Section_0= ruleSection | this_Page_1= rulePage )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==30) ) {
                alt13=1;
            }
            else if ( (LA13_0==31) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalPxManual.g:1303:3: this_Section_0= ruleSection
                    {

                    			newCompositeNode(grammarAccess.getAbstractSectionAccess().getSectionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Section_0=ruleSection();

                    state._fsp--;


                    			current = this_Section_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPxManual.g:1312:3: this_Page_1= rulePage
                    {

                    			newCompositeNode(grammarAccess.getAbstractSectionAccess().getPageParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Page_1=rulePage();

                    state._fsp--;


                    			current = this_Page_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractSection"


    // $ANTLR start "entryRuleSection"
    // InternalPxManual.g:1324:1: entryRuleSection returns [EObject current=null] : iv_ruleSection= ruleSection EOF ;
    public final EObject entryRuleSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSection = null;


        try {
            // InternalPxManual.g:1324:48: (iv_ruleSection= ruleSection EOF )
            // InternalPxManual.g:1325:2: iv_ruleSection= ruleSection EOF
            {
             newCompositeNode(grammarAccess.getSectionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSection=ruleSection();

            state._fsp--;

             current =iv_ruleSection; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSection"


    // $ANTLR start "ruleSection"
    // InternalPxManual.g:1331:1: ruleSection returns [EObject current=null] : (otherlv_0= 'section' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' ) ;
    public final EObject ruleSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_title_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1337:2: ( (otherlv_0= 'section' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' ) )
            // InternalPxManual.g:1338:2: (otherlv_0= 'section' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' )
            {
            // InternalPxManual.g:1338:2: (otherlv_0= 'section' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' )
            // InternalPxManual.g:1339:3: otherlv_0= 'section' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_17); 

            			newLeafNode(otherlv_0, grammarAccess.getSectionAccess().getSectionKeyword_0());
            		
            // InternalPxManual.g:1343:3: ( (lv_name_1_0= RULE_ID ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPxManual.g:1344:4: (lv_name_1_0= RULE_ID )
                    {
                    // InternalPxManual.g:1344:4: (lv_name_1_0= RULE_ID )
                    // InternalPxManual.g:1345:5: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_18); 

                    					newLeafNode(lv_name_1_0, grammarAccess.getSectionAccess().getNameIDTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSectionRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_1_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }
                    break;

            }

            // InternalPxManual.g:1361:3: ( (lv_title_2_0= ruleText ) )
            // InternalPxManual.g:1362:4: (lv_title_2_0= ruleText )
            {
            // InternalPxManual.g:1362:4: (lv_title_2_0= ruleText )
            // InternalPxManual.g:1363:5: lv_title_2_0= ruleText
            {

            					newCompositeNode(grammarAccess.getSectionAccess().getTitleTextParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_8);
            lv_title_2_0=ruleText();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSectionRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.Text");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getSectionAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPxManual.g:1384:3: ( (lv_elements_4_0= ruleManualElement ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_STRING||LA15_0==16||LA15_0==18||(LA15_0>=20 && LA15_0<=21)||(LA15_0>=29 && LA15_0<=32)||(LA15_0>=34 && LA15_0<=37)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalPxManual.g:1385:4: (lv_elements_4_0= ruleManualElement )
            	    {
            	    // InternalPxManual.g:1385:4: (lv_elements_4_0= ruleManualElement )
            	    // InternalPxManual.g:1386:5: lv_elements_4_0= ruleManualElement
            	    {

            	    					newCompositeNode(grammarAccess.getSectionAccess().getElementsManualElementParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_elements_4_0=ruleManualElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSectionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_4_0,
            	    						"org.pragmaticmodeling.pxmanual.PxManual.ManualElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getSectionAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSection"


    // $ANTLR start "entryRulePage"
    // InternalPxManual.g:1411:1: entryRulePage returns [EObject current=null] : iv_rulePage= rulePage EOF ;
    public final EObject entryRulePage() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePage = null;


        try {
            // InternalPxManual.g:1411:45: (iv_rulePage= rulePage EOF )
            // InternalPxManual.g:1412:2: iv_rulePage= rulePage EOF
            {
             newCompositeNode(grammarAccess.getPageRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePage=rulePage();

            state._fsp--;

             current =iv_rulePage; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePage"


    // $ANTLR start "rulePage"
    // InternalPxManual.g:1418:1: rulePage returns [EObject current=null] : (otherlv_0= 'page' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' ) ;
    public final EObject rulePage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_title_2_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1424:2: ( (otherlv_0= 'page' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' ) )
            // InternalPxManual.g:1425:2: (otherlv_0= 'page' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' )
            {
            // InternalPxManual.g:1425:2: (otherlv_0= 'page' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}' )
            // InternalPxManual.g:1426:3: otherlv_0= 'page' ( (lv_name_1_0= RULE_ID ) )? ( (lv_title_2_0= ruleText ) ) otherlv_3= '{' ( (lv_elements_4_0= ruleManualElement ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_17); 

            			newLeafNode(otherlv_0, grammarAccess.getPageAccess().getPageKeyword_0());
            		
            // InternalPxManual.g:1430:3: ( (lv_name_1_0= RULE_ID ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalPxManual.g:1431:4: (lv_name_1_0= RULE_ID )
                    {
                    // InternalPxManual.g:1431:4: (lv_name_1_0= RULE_ID )
                    // InternalPxManual.g:1432:5: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_18); 

                    					newLeafNode(lv_name_1_0, grammarAccess.getPageAccess().getNameIDTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPageRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_1_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }
                    break;

            }

            // InternalPxManual.g:1448:3: ( (lv_title_2_0= ruleText ) )
            // InternalPxManual.g:1449:4: (lv_title_2_0= ruleText )
            {
            // InternalPxManual.g:1449:4: (lv_title_2_0= ruleText )
            // InternalPxManual.g:1450:5: lv_title_2_0= ruleText
            {

            					newCompositeNode(grammarAccess.getPageAccess().getTitleTextParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_8);
            lv_title_2_0=ruleText();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPageRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.Text");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getPageAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPxManual.g:1471:3: ( (lv_elements_4_0= ruleManualElement ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==RULE_STRING||LA17_0==16||LA17_0==18||(LA17_0>=20 && LA17_0<=21)||(LA17_0>=29 && LA17_0<=32)||(LA17_0>=34 && LA17_0<=37)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalPxManual.g:1472:4: (lv_elements_4_0= ruleManualElement )
            	    {
            	    // InternalPxManual.g:1472:4: (lv_elements_4_0= ruleManualElement )
            	    // InternalPxManual.g:1473:5: lv_elements_4_0= ruleManualElement
            	    {

            	    					newCompositeNode(grammarAccess.getPageAccess().getElementsManualElementParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_elements_4_0=ruleManualElement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPageRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_4_0,
            	    						"org.pragmaticmodeling.pxmanual.PxManual.ManualElement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getPageAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePage"


    // $ANTLR start "entryRuleAbstractExample"
    // InternalPxManual.g:1498:1: entryRuleAbstractExample returns [EObject current=null] : iv_ruleAbstractExample= ruleAbstractExample EOF ;
    public final EObject entryRuleAbstractExample() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractExample = null;


        try {
            // InternalPxManual.g:1498:56: (iv_ruleAbstractExample= ruleAbstractExample EOF )
            // InternalPxManual.g:1499:2: iv_ruleAbstractExample= ruleAbstractExample EOF
            {
             newCompositeNode(grammarAccess.getAbstractExampleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractExample=ruleAbstractExample();

            state._fsp--;

             current =iv_ruleAbstractExample; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractExample"


    // $ANTLR start "ruleAbstractExample"
    // InternalPxManual.g:1505:1: ruleAbstractExample returns [EObject current=null] : (this_Example_0= ruleExample | this_Usage_1= ruleUsage ) ;
    public final EObject ruleAbstractExample() throws RecognitionException {
        EObject current = null;

        EObject this_Example_0 = null;

        EObject this_Usage_1 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1511:2: ( (this_Example_0= ruleExample | this_Usage_1= ruleUsage ) )
            // InternalPxManual.g:1512:2: (this_Example_0= ruleExample | this_Usage_1= ruleUsage )
            {
            // InternalPxManual.g:1512:2: (this_Example_0= ruleExample | this_Usage_1= ruleUsage )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==32) ) {
                alt18=1;
            }
            else if ( (LA18_0==34) ) {
                alt18=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalPxManual.g:1513:3: this_Example_0= ruleExample
                    {

                    			newCompositeNode(grammarAccess.getAbstractExampleAccess().getExampleParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Example_0=ruleExample();

                    state._fsp--;


                    			current = this_Example_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPxManual.g:1522:3: this_Usage_1= ruleUsage
                    {

                    			newCompositeNode(grammarAccess.getAbstractExampleAccess().getUsageParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Usage_1=ruleUsage();

                    state._fsp--;


                    			current = this_Usage_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractExample"


    // $ANTLR start "entryRuleExample"
    // InternalPxManual.g:1534:1: entryRuleExample returns [EObject current=null] : iv_ruleExample= ruleExample EOF ;
    public final EObject entryRuleExample() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExample = null;


        try {
            // InternalPxManual.g:1534:48: (iv_ruleExample= ruleExample EOF )
            // InternalPxManual.g:1535:2: iv_ruleExample= ruleExample EOF
            {
             newCompositeNode(grammarAccess.getExampleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExample=ruleExample();

            state._fsp--;

             current =iv_ruleExample; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExample"


    // $ANTLR start "ruleExample"
    // InternalPxManual.g:1541:1: ruleExample returns [EObject current=null] : (otherlv_0= 'example' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) ) ;
    public final EObject ruleExample() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token lv_screenshot_4_0=null;
        EObject lv_title_2_0 = null;

        EObject lv_body_5_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1547:2: ( (otherlv_0= 'example' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) ) )
            // InternalPxManual.g:1548:2: (otherlv_0= 'example' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) )
            {
            // InternalPxManual.g:1548:2: (otherlv_0= 'example' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) )
            // InternalPxManual.g:1549:3: otherlv_0= 'example' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) )
            {
            otherlv_0=(Token)match(input,32,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getExampleAccess().getExampleKeyword_0());
            		
            // InternalPxManual.g:1553:3: ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:1554:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:1554:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:1555:5: ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getExampleAccess().getUnorderedGroup_1());
            				
            // InternalPxManual.g:1558:5: ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* )
            // InternalPxManual.g:1559:6: ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:1559:6: ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )*
            loop19:
            do {
                int alt19=3;
                int LA19_0 = input.LA(1);

                if ( ( LA19_0 == RULE_STRING || LA19_0 >= 35 && LA19_0 <= 36 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0) ) {
                    alt19=1;
                }
                else if ( LA19_0 == 33 && getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1) ) {
                    alt19=2;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalPxManual.g:1560:4: ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1560:4: ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) )
            	    // InternalPxManual.g:1561:5: {...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleExample", "getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0)");
            	    }
            	    // InternalPxManual.g:1561:104: ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) )
            	    // InternalPxManual.g:1562:6: ({...}? => ( (lv_title_2_0= ruleText ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 0);
            	    					
            	    // InternalPxManual.g:1565:9: ({...}? => ( (lv_title_2_0= ruleText ) ) )
            	    // InternalPxManual.g:1565:10: {...}? => ( (lv_title_2_0= ruleText ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleExample", "true");
            	    }
            	    // InternalPxManual.g:1565:19: ( (lv_title_2_0= ruleText ) )
            	    // InternalPxManual.g:1565:20: (lv_title_2_0= ruleText )
            	    {
            	    // InternalPxManual.g:1565:20: (lv_title_2_0= ruleText )
            	    // InternalPxManual.g:1566:10: lv_title_2_0= ruleText
            	    {

            	    										newCompositeNode(grammarAccess.getExampleAccess().getTitleTextParserRuleCall_1_0_0());
            	    									
            	    pushFollow(FOLLOW_19);
            	    lv_title_2_0=ruleText();

            	    state._fsp--;


            	    										if (current==null) {
            	    											current = createModelElementForParent(grammarAccess.getExampleRule());
            	    										}
            	    										set(
            	    											current,
            	    											"title",
            	    											lv_title_2_0,
            	    											"org.pragmaticmodeling.pxmanual.PxManual.Text");
            	    										afterParserOrEnumRuleCall();
            	    									

            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getExampleAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:1588:4: ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1588:4: ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:1589:5: {...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleExample", "getUnorderedGroupHelper().canSelect(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1)");
            	    }
            	    // InternalPxManual.g:1589:104: ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:1590:6: ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getExampleAccess().getUnorderedGroup_1(), 1);
            	    					
            	    // InternalPxManual.g:1593:9: ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:1593:10: {...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleExample", "true");
            	    }
            	    // InternalPxManual.g:1593:19: (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:1593:20: otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) )
            	    {
            	    otherlv_3=(Token)match(input,33,FOLLOW_4); 

            	    									newLeafNode(otherlv_3, grammarAccess.getExampleAccess().getScreenshotKeyword_1_1_0());
            	    								
            	    // InternalPxManual.g:1597:9: ( (lv_screenshot_4_0= RULE_STRING ) )
            	    // InternalPxManual.g:1598:10: (lv_screenshot_4_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:1598:10: (lv_screenshot_4_0= RULE_STRING )
            	    // InternalPxManual.g:1599:11: lv_screenshot_4_0= RULE_STRING
            	    {
            	    lv_screenshot_4_0=(Token)match(input,RULE_STRING,FOLLOW_19); 

            	    											newLeafNode(lv_screenshot_4_0, grammarAccess.getExampleAccess().getScreenshotSTRINGTerminalRuleCall_1_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getExampleRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"screenshot",
            	    												lv_screenshot_4_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getExampleAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getExampleAccess().getUnorderedGroup_1());
            				

            }

            // InternalPxManual.g:1628:3: ( (lv_body_5_0= ruleDocBody ) )
            // InternalPxManual.g:1629:4: (lv_body_5_0= ruleDocBody )
            {
            // InternalPxManual.g:1629:4: (lv_body_5_0= ruleDocBody )
            // InternalPxManual.g:1630:5: lv_body_5_0= ruleDocBody
            {

            					newCompositeNode(grammarAccess.getExampleAccess().getBodyDocBodyParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_5_0=ruleDocBody();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExampleRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_5_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExample"


    // $ANTLR start "entryRuleUsage"
    // InternalPxManual.g:1651:1: entryRuleUsage returns [EObject current=null] : iv_ruleUsage= ruleUsage EOF ;
    public final EObject entryRuleUsage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUsage = null;


        try {
            // InternalPxManual.g:1651:46: (iv_ruleUsage= ruleUsage EOF )
            // InternalPxManual.g:1652:2: iv_ruleUsage= ruleUsage EOF
            {
             newCompositeNode(grammarAccess.getUsageRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUsage=ruleUsage();

            state._fsp--;

             current =iv_ruleUsage; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUsage"


    // $ANTLR start "ruleUsage"
    // InternalPxManual.g:1658:1: ruleUsage returns [EObject current=null] : (otherlv_0= 'usage' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) ) ;
    public final EObject ruleUsage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token lv_screenshot_4_0=null;
        EObject lv_title_2_0 = null;

        EObject lv_body_5_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1664:2: ( (otherlv_0= 'usage' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) ) )
            // InternalPxManual.g:1665:2: (otherlv_0= 'usage' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) )
            {
            // InternalPxManual.g:1665:2: (otherlv_0= 'usage' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) ) )
            // InternalPxManual.g:1666:3: otherlv_0= 'usage' ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) ) ( (lv_body_5_0= ruleDocBody ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getUsageAccess().getUsageKeyword_0());
            		
            // InternalPxManual.g:1670:3: ( ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:1671:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:1671:4: ( ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:1672:5: ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getUsageAccess().getUnorderedGroup_1());
            				
            // InternalPxManual.g:1675:5: ( ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )* )
            // InternalPxManual.g:1676:6: ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:1676:6: ( ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) ) )*
            loop20:
            do {
                int alt20=3;
                int LA20_0 = input.LA(1);

                if ( ( LA20_0 == RULE_STRING || LA20_0 >= 35 && LA20_0 <= 36 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0) ) {
                    alt20=1;
                }
                else if ( LA20_0 == 33 && getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1) ) {
                    alt20=2;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalPxManual.g:1677:4: ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1677:4: ({...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) ) )
            	    // InternalPxManual.g:1678:5: {...}? => ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleUsage", "getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0)");
            	    }
            	    // InternalPxManual.g:1678:102: ( ({...}? => ( (lv_title_2_0= ruleText ) ) ) )
            	    // InternalPxManual.g:1679:6: ({...}? => ( (lv_title_2_0= ruleText ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 0);
            	    					
            	    // InternalPxManual.g:1682:9: ({...}? => ( (lv_title_2_0= ruleText ) ) )
            	    // InternalPxManual.g:1682:10: {...}? => ( (lv_title_2_0= ruleText ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleUsage", "true");
            	    }
            	    // InternalPxManual.g:1682:19: ( (lv_title_2_0= ruleText ) )
            	    // InternalPxManual.g:1682:20: (lv_title_2_0= ruleText )
            	    {
            	    // InternalPxManual.g:1682:20: (lv_title_2_0= ruleText )
            	    // InternalPxManual.g:1683:10: lv_title_2_0= ruleText
            	    {

            	    										newCompositeNode(grammarAccess.getUsageAccess().getTitleTextParserRuleCall_1_0_0());
            	    									
            	    pushFollow(FOLLOW_19);
            	    lv_title_2_0=ruleText();

            	    state._fsp--;


            	    										if (current==null) {
            	    											current = createModelElementForParent(grammarAccess.getUsageRule());
            	    										}
            	    										set(
            	    											current,
            	    											"title",
            	    											lv_title_2_0,
            	    											"org.pragmaticmodeling.pxmanual.PxManual.Text");
            	    										afterParserOrEnumRuleCall();
            	    									

            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getUsageAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:1705:4: ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1705:4: ({...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) ) )
            	    // InternalPxManual.g:1706:5: {...}? => ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleUsage", "getUnorderedGroupHelper().canSelect(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1)");
            	    }
            	    // InternalPxManual.g:1706:102: ( ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) ) )
            	    // InternalPxManual.g:1707:6: ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getUsageAccess().getUnorderedGroup_1(), 1);
            	    					
            	    // InternalPxManual.g:1710:9: ({...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) ) )
            	    // InternalPxManual.g:1710:10: {...}? => (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleUsage", "true");
            	    }
            	    // InternalPxManual.g:1710:19: (otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) ) )
            	    // InternalPxManual.g:1710:20: otherlv_3= 'screenshot:' ( (lv_screenshot_4_0= RULE_STRING ) )
            	    {
            	    otherlv_3=(Token)match(input,33,FOLLOW_4); 

            	    									newLeafNode(otherlv_3, grammarAccess.getUsageAccess().getScreenshotKeyword_1_1_0());
            	    								
            	    // InternalPxManual.g:1714:9: ( (lv_screenshot_4_0= RULE_STRING ) )
            	    // InternalPxManual.g:1715:10: (lv_screenshot_4_0= RULE_STRING )
            	    {
            	    // InternalPxManual.g:1715:10: (lv_screenshot_4_0= RULE_STRING )
            	    // InternalPxManual.g:1716:11: lv_screenshot_4_0= RULE_STRING
            	    {
            	    lv_screenshot_4_0=(Token)match(input,RULE_STRING,FOLLOW_19); 

            	    											newLeafNode(lv_screenshot_4_0, grammarAccess.getUsageAccess().getScreenshotSTRINGTerminalRuleCall_1_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getUsageRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"screenshot",
            	    												lv_screenshot_4_0,
            	    												"org.eclipse.xtext.common.Terminals.STRING");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getUsageAccess().getUnorderedGroup_1());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getUsageAccess().getUnorderedGroup_1());
            				

            }

            // InternalPxManual.g:1745:3: ( (lv_body_5_0= ruleDocBody ) )
            // InternalPxManual.g:1746:4: (lv_body_5_0= ruleDocBody )
            {
            // InternalPxManual.g:1746:4: (lv_body_5_0= ruleDocBody )
            // InternalPxManual.g:1747:5: lv_body_5_0= ruleDocBody
            {

            					newCompositeNode(grammarAccess.getUsageAccess().getBodyDocBodyParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_body_5_0=ruleDocBody();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUsageRule());
            					}
            					set(
            						current,
            						"body",
            						lv_body_5_0,
            						"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUsage"


    // $ANTLR start "entryRuleText"
    // InternalPxManual.g:1768:1: entryRuleText returns [EObject current=null] : iv_ruleText= ruleText EOF ;
    public final EObject entryRuleText() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleText = null;


        try {
            // InternalPxManual.g:1768:45: (iv_ruleText= ruleText EOF )
            // InternalPxManual.g:1769:2: iv_ruleText= ruleText EOF
            {
             newCompositeNode(grammarAccess.getTextRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleText=ruleText();

            state._fsp--;

             current =iv_ruleText; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleText"


    // $ANTLR start "ruleText"
    // InternalPxManual.g:1775:1: ruleText returns [EObject current=null] : ( (otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) ) )? (otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) ) )? ( (lv_body_4_0= RULE_STRING ) ) ) ;
    public final EObject ruleText() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_language_1_0=null;
        Token otherlv_2=null;
        Token lv_className_3_0=null;
        Token lv_body_4_0=null;


        	enterRule();

        try {
            // InternalPxManual.g:1781:2: ( ( (otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) ) )? (otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) ) )? ( (lv_body_4_0= RULE_STRING ) ) ) )
            // InternalPxManual.g:1782:2: ( (otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) ) )? (otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) ) )? ( (lv_body_4_0= RULE_STRING ) ) )
            {
            // InternalPxManual.g:1782:2: ( (otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) ) )? (otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) ) )? ( (lv_body_4_0= RULE_STRING ) ) )
            // InternalPxManual.g:1783:3: (otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) ) )? (otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) ) )? ( (lv_body_4_0= RULE_STRING ) )
            {
            // InternalPxManual.g:1783:3: (otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==35) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPxManual.g:1784:4: otherlv_0= '@' ( (lv_language_1_0= RULE_ID ) )
                    {
                    otherlv_0=(Token)match(input,35,FOLLOW_20); 

                    				newLeafNode(otherlv_0, grammarAccess.getTextAccess().getCommercialAtKeyword_0_0());
                    			
                    // InternalPxManual.g:1788:4: ( (lv_language_1_0= RULE_ID ) )
                    // InternalPxManual.g:1789:5: (lv_language_1_0= RULE_ID )
                    {
                    // InternalPxManual.g:1789:5: (lv_language_1_0= RULE_ID )
                    // InternalPxManual.g:1790:6: lv_language_1_0= RULE_ID
                    {
                    lv_language_1_0=(Token)match(input,RULE_ID,FOLLOW_21); 

                    						newLeafNode(lv_language_1_0, grammarAccess.getTextAccess().getLanguageIDTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTextRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"language",
                    							lv_language_1_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPxManual.g:1807:3: (otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==36) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalPxManual.g:1808:4: otherlv_2= 'class:' ( (lv_className_3_0= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,36,FOLLOW_20); 

                    				newLeafNode(otherlv_2, grammarAccess.getTextAccess().getClassKeyword_1_0());
                    			
                    // InternalPxManual.g:1812:4: ( (lv_className_3_0= RULE_ID ) )
                    // InternalPxManual.g:1813:5: (lv_className_3_0= RULE_ID )
                    {
                    // InternalPxManual.g:1813:5: (lv_className_3_0= RULE_ID )
                    // InternalPxManual.g:1814:6: lv_className_3_0= RULE_ID
                    {
                    lv_className_3_0=(Token)match(input,RULE_ID,FOLLOW_4); 

                    						newLeafNode(lv_className_3_0, grammarAccess.getTextAccess().getClassNameIDTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getTextRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"className",
                    							lv_className_3_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPxManual.g:1831:3: ( (lv_body_4_0= RULE_STRING ) )
            // InternalPxManual.g:1832:4: (lv_body_4_0= RULE_STRING )
            {
            // InternalPxManual.g:1832:4: (lv_body_4_0= RULE_STRING )
            // InternalPxManual.g:1833:5: lv_body_4_0= RULE_STRING
            {
            lv_body_4_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_body_4_0, grammarAccess.getTextAccess().getBodySTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTextRule());
            					}
            					setWithLastConsumed(
            						current,
            						"body",
            						lv_body_4_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleText"


    // $ANTLR start "entryRuleKeyword"
    // InternalPxManual.g:1853:1: entryRuleKeyword returns [EObject current=null] : iv_ruleKeyword= ruleKeyword EOF ;
    public final EObject entryRuleKeyword() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleKeyword = null;


        try {
            // InternalPxManual.g:1853:48: (iv_ruleKeyword= ruleKeyword EOF )
            // InternalPxManual.g:1854:2: iv_ruleKeyword= ruleKeyword EOF
            {
             newCompositeNode(grammarAccess.getKeywordRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleKeyword=ruleKeyword();

            state._fsp--;

             current =iv_ruleKeyword; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKeyword"


    // $ANTLR start "ruleKeyword"
    // InternalPxManual.g:1860:1: ruleKeyword returns [EObject current=null] : (otherlv_0= 'keyword' ( (lv_name_1_0= RULE_ID ) )? ( (lv_keywordName_2_0= RULE_STRING ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_13= '}' ) ;
    public final EObject ruleKeyword() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_keywordName_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_documentation_6_0 = null;

        EObject lv_prettyTitle_8_0 = null;

        EObject lv_examples_9_0 = null;

        EObject lv_attributes_10_0 = null;

        EObject lv_conclusion_12_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:1866:2: ( (otherlv_0= 'keyword' ( (lv_name_1_0= RULE_ID ) )? ( (lv_keywordName_2_0= RULE_STRING ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_13= '}' ) )
            // InternalPxManual.g:1867:2: (otherlv_0= 'keyword' ( (lv_name_1_0= RULE_ID ) )? ( (lv_keywordName_2_0= RULE_STRING ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_13= '}' )
            {
            // InternalPxManual.g:1867:2: (otherlv_0= 'keyword' ( (lv_name_1_0= RULE_ID ) )? ( (lv_keywordName_2_0= RULE_STRING ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_13= '}' )
            // InternalPxManual.g:1868:3: otherlv_0= 'keyword' ( (lv_name_1_0= RULE_ID ) )? ( (lv_keywordName_2_0= RULE_STRING ) ) otherlv_3= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_13= '}'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getKeywordAccess().getKeywordKeyword_0());
            		
            // InternalPxManual.g:1872:3: ( (lv_name_1_0= RULE_ID ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_ID) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalPxManual.g:1873:4: (lv_name_1_0= RULE_ID )
                    {
                    // InternalPxManual.g:1873:4: (lv_name_1_0= RULE_ID )
                    // InternalPxManual.g:1874:5: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

                    					newLeafNode(lv_name_1_0, grammarAccess.getKeywordAccess().getNameIDTerminalRuleCall_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getKeywordRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_1_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }
                    break;

            }

            // InternalPxManual.g:1890:3: ( (lv_keywordName_2_0= RULE_STRING ) )
            // InternalPxManual.g:1891:4: (lv_keywordName_2_0= RULE_STRING )
            {
            // InternalPxManual.g:1891:4: (lv_keywordName_2_0= RULE_STRING )
            // InternalPxManual.g:1892:5: lv_keywordName_2_0= RULE_STRING
            {
            lv_keywordName_2_0=(Token)match(input,RULE_STRING,FOLLOW_8); 

            					newLeafNode(lv_keywordName_2_0, grammarAccess.getKeywordAccess().getKeywordNameSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getKeywordRule());
            					}
            					setWithLastConsumed(
            						current,
            						"keywordName",
            						lv_keywordName_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_3=(Token)match(input,14,FOLLOW_22); 

            			newLeafNode(otherlv_3, grammarAccess.getKeywordAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPxManual.g:1912:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:1913:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:1913:4: ( ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:1914:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            				
            // InternalPxManual.g:1917:5: ( ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )* )
            // InternalPxManual.g:1918:6: ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:1918:6: ( ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) ) | ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) ) )*
            loop26:
            do {
                int alt26=6;
                int LA26_0 = input.LA(1);

                if ( LA26_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
                    alt26=1;
                }
                else if ( LA26_0 == 39 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
                    alt26=2;
                }
                else if ( ( LA26_0 == 32 || LA26_0 == 34 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
                    alt26=3;
                }
                else if ( LA26_0 == 41 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
                    alt26=4;
                }
                else if ( LA26_0 == 40 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
                    alt26=5;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalPxManual.g:1919:4: ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1919:4: ({...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) ) )
            	    // InternalPxManual.g:1920:5: {...}? => ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0)");
            	    }
            	    // InternalPxManual.g:1920:104: ( ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) ) )
            	    // InternalPxManual.g:1921:6: ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 0);
            	    					
            	    // InternalPxManual.g:1924:9: ({...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) ) )
            	    // InternalPxManual.g:1924:10: {...}? => (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "true");
            	    }
            	    // InternalPxManual.g:1924:19: (otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) ) )
            	    // InternalPxManual.g:1924:20: otherlv_5= 'doc:' ( (lv_documentation_6_0= ruleDocBody ) )
            	    {
            	    otherlv_5=(Token)match(input,38,FOLLOW_3); 

            	    									newLeafNode(otherlv_5, grammarAccess.getKeywordAccess().getDocKeyword_4_0_0());
            	    								
            	    // InternalPxManual.g:1928:9: ( (lv_documentation_6_0= ruleDocBody ) )
            	    // InternalPxManual.g:1929:10: (lv_documentation_6_0= ruleDocBody )
            	    {
            	    // InternalPxManual.g:1929:10: (lv_documentation_6_0= ruleDocBody )
            	    // InternalPxManual.g:1930:11: lv_documentation_6_0= ruleDocBody
            	    {

            	    											newCompositeNode(grammarAccess.getKeywordAccess().getDocumentationDocBodyParserRuleCall_4_0_1_0());
            	    										
            	    pushFollow(FOLLOW_22);
            	    lv_documentation_6_0=ruleDocBody();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getKeywordRule());
            	    											}
            	    											set(
            	    												current,
            	    												"documentation",
            	    												lv_documentation_6_0,
            	    												"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:1953:4: ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:1953:4: ({...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) ) )
            	    // InternalPxManual.g:1954:5: {...}? => ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1)");
            	    }
            	    // InternalPxManual.g:1954:104: ( ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) ) )
            	    // InternalPxManual.g:1955:6: ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 1);
            	    					
            	    // InternalPxManual.g:1958:9: ({...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) ) )
            	    // InternalPxManual.g:1958:10: {...}? => (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "true");
            	    }
            	    // InternalPxManual.g:1958:19: (otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) ) )
            	    // InternalPxManual.g:1958:20: otherlv_7= 'prettyTitle:' ( (lv_prettyTitle_8_0= ruleText ) )
            	    {
            	    otherlv_7=(Token)match(input,39,FOLLOW_18); 

            	    									newLeafNode(otherlv_7, grammarAccess.getKeywordAccess().getPrettyTitleKeyword_4_1_0());
            	    								
            	    // InternalPxManual.g:1962:9: ( (lv_prettyTitle_8_0= ruleText ) )
            	    // InternalPxManual.g:1963:10: (lv_prettyTitle_8_0= ruleText )
            	    {
            	    // InternalPxManual.g:1963:10: (lv_prettyTitle_8_0= ruleText )
            	    // InternalPxManual.g:1964:11: lv_prettyTitle_8_0= ruleText
            	    {

            	    											newCompositeNode(grammarAccess.getKeywordAccess().getPrettyTitleTextParserRuleCall_4_1_1_0());
            	    										
            	    pushFollow(FOLLOW_22);
            	    lv_prettyTitle_8_0=ruleText();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getKeywordRule());
            	    											}
            	    											set(
            	    												current,
            	    												"prettyTitle",
            	    												lv_prettyTitle_8_0,
            	    												"org.pragmaticmodeling.pxmanual.PxManual.Text");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalPxManual.g:1987:4: ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) )
            	    {
            	    // InternalPxManual.g:1987:4: ({...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ ) )
            	    // InternalPxManual.g:1988:5: {...}? => ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2)");
            	    }
            	    // InternalPxManual.g:1988:104: ( ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+ )
            	    // InternalPxManual.g:1989:6: ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 2);
            	    					
            	    // InternalPxManual.g:1992:9: ({...}? => ( (lv_examples_9_0= ruleAbstractExample ) ) )+
            	    int cnt24=0;
            	    loop24:
            	    do {
            	        int alt24=2;
            	        int LA24_0 = input.LA(1);

            	        if ( (LA24_0==32) ) {
            	            int LA24_2 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt24=1;
            	            }


            	        }
            	        else if ( (LA24_0==34) ) {
            	            int LA24_3 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt24=1;
            	            }


            	        }


            	        switch (alt24) {
            	    	case 1 :
            	    	    // InternalPxManual.g:1992:10: {...}? => ( (lv_examples_9_0= ruleAbstractExample ) )
            	    	    {
            	    	    if ( !((true)) ) {
            	    	        throw new FailedPredicateException(input, "ruleKeyword", "true");
            	    	    }
            	    	    // InternalPxManual.g:1992:19: ( (lv_examples_9_0= ruleAbstractExample ) )
            	    	    // InternalPxManual.g:1992:20: (lv_examples_9_0= ruleAbstractExample )
            	    	    {
            	    	    // InternalPxManual.g:1992:20: (lv_examples_9_0= ruleAbstractExample )
            	    	    // InternalPxManual.g:1993:10: lv_examples_9_0= ruleAbstractExample
            	    	    {

            	    	    										newCompositeNode(grammarAccess.getKeywordAccess().getExamplesAbstractExampleParserRuleCall_4_2_0());
            	    	    									
            	    	    pushFollow(FOLLOW_22);
            	    	    lv_examples_9_0=ruleAbstractExample();

            	    	    state._fsp--;


            	    	    										if (current==null) {
            	    	    											current = createModelElementForParent(grammarAccess.getKeywordRule());
            	    	    										}
            	    	    										add(
            	    	    											current,
            	    	    											"examples",
            	    	    											lv_examples_9_0,
            	    	    											"org.pragmaticmodeling.pxmanual.PxManual.AbstractExample");
            	    	    										afterParserOrEnumRuleCall();
            	    	    									

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt24 >= 1 ) break loop24;
            	                EarlyExitException eee =
            	                    new EarlyExitException(24, input);
            	                throw eee;
            	        }
            	        cnt24++;
            	    } while (true);

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalPxManual.g:2015:4: ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) )
            	    {
            	    // InternalPxManual.g:2015:4: ({...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ ) )
            	    // InternalPxManual.g:2016:5: {...}? => ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3)");
            	    }
            	    // InternalPxManual.g:2016:104: ( ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+ )
            	    // InternalPxManual.g:2017:6: ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 3);
            	    					
            	    // InternalPxManual.g:2020:9: ({...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) ) )+
            	    int cnt25=0;
            	    loop25:
            	    do {
            	        int alt25=2;
            	        int LA25_0 = input.LA(1);

            	        if ( (LA25_0==41) ) {
            	            int LA25_2 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt25=1;
            	            }


            	        }


            	        switch (alt25) {
            	    	case 1 :
            	    	    // InternalPxManual.g:2020:10: {...}? => ( (lv_attributes_10_0= ruleKeywordAttribute ) )
            	    	    {
            	    	    if ( !((true)) ) {
            	    	        throw new FailedPredicateException(input, "ruleKeyword", "true");
            	    	    }
            	    	    // InternalPxManual.g:2020:19: ( (lv_attributes_10_0= ruleKeywordAttribute ) )
            	    	    // InternalPxManual.g:2020:20: (lv_attributes_10_0= ruleKeywordAttribute )
            	    	    {
            	    	    // InternalPxManual.g:2020:20: (lv_attributes_10_0= ruleKeywordAttribute )
            	    	    // InternalPxManual.g:2021:10: lv_attributes_10_0= ruleKeywordAttribute
            	    	    {

            	    	    										newCompositeNode(grammarAccess.getKeywordAccess().getAttributesKeywordAttributeParserRuleCall_4_3_0());
            	    	    									
            	    	    pushFollow(FOLLOW_22);
            	    	    lv_attributes_10_0=ruleKeywordAttribute();

            	    	    state._fsp--;


            	    	    										if (current==null) {
            	    	    											current = createModelElementForParent(grammarAccess.getKeywordRule());
            	    	    										}
            	    	    										add(
            	    	    											current,
            	    	    											"attributes",
            	    	    											lv_attributes_10_0,
            	    	    											"org.pragmaticmodeling.pxmanual.PxManual.KeywordAttribute");
            	    	    										afterParserOrEnumRuleCall();
            	    	    									

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt25 >= 1 ) break loop25;
            	                EarlyExitException eee =
            	                    new EarlyExitException(25, input);
            	                throw eee;
            	        }
            	        cnt25++;
            	    } while (true);

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 5 :
            	    // InternalPxManual.g:2043:4: ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:2043:4: ({...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) ) )
            	    // InternalPxManual.g:2044:5: {...}? => ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4)");
            	    }
            	    // InternalPxManual.g:2044:104: ( ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) ) )
            	    // InternalPxManual.g:2045:6: ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAccess().getUnorderedGroup_4(), 4);
            	    					
            	    // InternalPxManual.g:2048:9: ({...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) ) )
            	    // InternalPxManual.g:2048:10: {...}? => (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleKeyword", "true");
            	    }
            	    // InternalPxManual.g:2048:19: (otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) ) )
            	    // InternalPxManual.g:2048:20: otherlv_11= 'conclusion:' ( (lv_conclusion_12_0= ruleDocBody ) )
            	    {
            	    otherlv_11=(Token)match(input,40,FOLLOW_3); 

            	    									newLeafNode(otherlv_11, grammarAccess.getKeywordAccess().getConclusionKeyword_4_4_0());
            	    								
            	    // InternalPxManual.g:2052:9: ( (lv_conclusion_12_0= ruleDocBody ) )
            	    // InternalPxManual.g:2053:10: (lv_conclusion_12_0= ruleDocBody )
            	    {
            	    // InternalPxManual.g:2053:10: (lv_conclusion_12_0= ruleDocBody )
            	    // InternalPxManual.g:2054:11: lv_conclusion_12_0= ruleDocBody
            	    {

            	    											newCompositeNode(grammarAccess.getKeywordAccess().getConclusionDocBodyParserRuleCall_4_4_1_0());
            	    										
            	    pushFollow(FOLLOW_22);
            	    lv_conclusion_12_0=ruleDocBody();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getKeywordRule());
            	    											}
            	    											set(
            	    												current,
            	    												"conclusion",
            	    												lv_conclusion_12_0,
            	    												"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getKeywordAccess().getUnorderedGroup_4());
            				

            }

            otherlv_13=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getKeywordAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKeyword"


    // $ANTLR start "entryRuleKeywordAttribute"
    // InternalPxManual.g:2092:1: entryRuleKeywordAttribute returns [EObject current=null] : iv_ruleKeywordAttribute= ruleKeywordAttribute EOF ;
    public final EObject entryRuleKeywordAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleKeywordAttribute = null;


        try {
            // InternalPxManual.g:2092:57: (iv_ruleKeywordAttribute= ruleKeywordAttribute EOF )
            // InternalPxManual.g:2093:2: iv_ruleKeywordAttribute= ruleKeywordAttribute EOF
            {
             newCompositeNode(grammarAccess.getKeywordAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleKeywordAttribute=ruleKeywordAttribute();

            state._fsp--;

             current =iv_ruleKeywordAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKeywordAttribute"


    // $ANTLR start "ruleKeywordAttribute"
    // InternalPxManual.g:2099:1: ruleKeywordAttribute returns [EObject current=null] : (otherlv_0= 'attribute' ( (lv_attName_1_0= RULE_STRING ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_10= '}' ) ;
    public final EObject ruleKeywordAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_attName_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_optional_7_0=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_documentation_5_0 = null;

        EObject lv_example_6_0 = null;

        EObject lv_possibleValues_9_0 = null;



        	enterRule();

        try {
            // InternalPxManual.g:2105:2: ( (otherlv_0= 'attribute' ( (lv_attName_1_0= RULE_STRING ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_10= '}' ) )
            // InternalPxManual.g:2106:2: (otherlv_0= 'attribute' ( (lv_attName_1_0= RULE_STRING ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_10= '}' )
            {
            // InternalPxManual.g:2106:2: (otherlv_0= 'attribute' ( (lv_attName_1_0= RULE_STRING ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_10= '}' )
            // InternalPxManual.g:2107:3: otherlv_0= 'attribute' ( (lv_attName_1_0= RULE_STRING ) ) otherlv_2= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) ) ) otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,41,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getKeywordAttributeAccess().getAttributeKeyword_0());
            		
            // InternalPxManual.g:2111:3: ( (lv_attName_1_0= RULE_STRING ) )
            // InternalPxManual.g:2112:4: (lv_attName_1_0= RULE_STRING )
            {
            // InternalPxManual.g:2112:4: (lv_attName_1_0= RULE_STRING )
            // InternalPxManual.g:2113:5: lv_attName_1_0= RULE_STRING
            {
            lv_attName_1_0=(Token)match(input,RULE_STRING,FOLLOW_8); 

            					newLeafNode(lv_attName_1_0, grammarAccess.getKeywordAttributeAccess().getAttNameSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getKeywordAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"attName",
            						lv_attName_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_23); 

            			newLeafNode(otherlv_2, grammarAccess.getKeywordAttributeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPxManual.g:2133:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) ) )
            // InternalPxManual.g:2134:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) )
            {
            // InternalPxManual.g:2134:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* ) )
            // InternalPxManual.g:2135:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            				
            // InternalPxManual.g:2138:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )* )
            // InternalPxManual.g:2139:6: ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )*
            {
            // InternalPxManual.g:2139:6: ( ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) ) | ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) ) | ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) ) )*
            loop28:
            do {
                int alt28=5;
                int LA28_0 = input.LA(1);

                if ( LA28_0 == 38 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
                    alt28=1;
                }
                else if ( ( LA28_0 == 32 || LA28_0 == 34 ) && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
                    alt28=2;
                }
                else if ( LA28_0 == 42 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
                    alt28=3;
                }
                else if ( LA28_0 == 43 && getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
                    alt28=4;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalPxManual.g:2140:4: ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:2140:4: ({...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) ) )
            	    // InternalPxManual.g:2141:5: {...}? => ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalPxManual.g:2141:113: ( ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) ) )
            	    // InternalPxManual.g:2142:6: ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 0);
            	    					
            	    // InternalPxManual.g:2145:9: ({...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) ) )
            	    // InternalPxManual.g:2145:10: {...}? => (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "true");
            	    }
            	    // InternalPxManual.g:2145:19: (otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) ) )
            	    // InternalPxManual.g:2145:20: otherlv_4= 'doc:' ( (lv_documentation_5_0= ruleDocBody ) )
            	    {
            	    otherlv_4=(Token)match(input,38,FOLLOW_3); 

            	    									newLeafNode(otherlv_4, grammarAccess.getKeywordAttributeAccess().getDocKeyword_3_0_0());
            	    								
            	    // InternalPxManual.g:2149:9: ( (lv_documentation_5_0= ruleDocBody ) )
            	    // InternalPxManual.g:2150:10: (lv_documentation_5_0= ruleDocBody )
            	    {
            	    // InternalPxManual.g:2150:10: (lv_documentation_5_0= ruleDocBody )
            	    // InternalPxManual.g:2151:11: lv_documentation_5_0= ruleDocBody
            	    {

            	    											newCompositeNode(grammarAccess.getKeywordAttributeAccess().getDocumentationDocBodyParserRuleCall_3_0_1_0());
            	    										
            	    pushFollow(FOLLOW_23);
            	    lv_documentation_5_0=ruleDocBody();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getKeywordAttributeRule());
            	    											}
            	    											set(
            	    												current,
            	    												"documentation",
            	    												lv_documentation_5_0,
            	    												"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalPxManual.g:2174:4: ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) )
            	    {
            	    // InternalPxManual.g:2174:4: ({...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ ) )
            	    // InternalPxManual.g:2175:5: {...}? => ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalPxManual.g:2175:113: ( ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+ )
            	    // InternalPxManual.g:2176:6: ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 1);
            	    					
            	    // InternalPxManual.g:2179:9: ({...}? => ( (lv_example_6_0= ruleAbstractExample ) ) )+
            	    int cnt27=0;
            	    loop27:
            	    do {
            	        int alt27=2;
            	        int LA27_0 = input.LA(1);

            	        if ( (LA27_0==32) ) {
            	            int LA27_2 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt27=1;
            	            }


            	        }
            	        else if ( (LA27_0==34) ) {
            	            int LA27_3 = input.LA(2);

            	            if ( ((true)) ) {
            	                alt27=1;
            	            }


            	        }


            	        switch (alt27) {
            	    	case 1 :
            	    	    // InternalPxManual.g:2179:10: {...}? => ( (lv_example_6_0= ruleAbstractExample ) )
            	    	    {
            	    	    if ( !((true)) ) {
            	    	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "true");
            	    	    }
            	    	    // InternalPxManual.g:2179:19: ( (lv_example_6_0= ruleAbstractExample ) )
            	    	    // InternalPxManual.g:2179:20: (lv_example_6_0= ruleAbstractExample )
            	    	    {
            	    	    // InternalPxManual.g:2179:20: (lv_example_6_0= ruleAbstractExample )
            	    	    // InternalPxManual.g:2180:10: lv_example_6_0= ruleAbstractExample
            	    	    {

            	    	    										newCompositeNode(grammarAccess.getKeywordAttributeAccess().getExampleAbstractExampleParserRuleCall_3_1_0());
            	    	    									
            	    	    pushFollow(FOLLOW_23);
            	    	    lv_example_6_0=ruleAbstractExample();

            	    	    state._fsp--;


            	    	    										if (current==null) {
            	    	    											current = createModelElementForParent(grammarAccess.getKeywordAttributeRule());
            	    	    										}
            	    	    										add(
            	    	    											current,
            	    	    											"example",
            	    	    											lv_example_6_0,
            	    	    											"org.pragmaticmodeling.pxmanual.PxManual.AbstractExample");
            	    	    										afterParserOrEnumRuleCall();
            	    	    									

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt27 >= 1 ) break loop27;
            	                EarlyExitException eee =
            	                    new EarlyExitException(27, input);
            	                throw eee;
            	        }
            	        cnt27++;
            	    } while (true);

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalPxManual.g:2202:4: ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) )
            	    {
            	    // InternalPxManual.g:2202:4: ({...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) ) )
            	    // InternalPxManual.g:2203:5: {...}? => ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalPxManual.g:2203:113: ( ({...}? => ( (lv_optional_7_0= 'optional' ) ) ) )
            	    // InternalPxManual.g:2204:6: ({...}? => ( (lv_optional_7_0= 'optional' ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 2);
            	    					
            	    // InternalPxManual.g:2207:9: ({...}? => ( (lv_optional_7_0= 'optional' ) ) )
            	    // InternalPxManual.g:2207:10: {...}? => ( (lv_optional_7_0= 'optional' ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "true");
            	    }
            	    // InternalPxManual.g:2207:19: ( (lv_optional_7_0= 'optional' ) )
            	    // InternalPxManual.g:2207:20: (lv_optional_7_0= 'optional' )
            	    {
            	    // InternalPxManual.g:2207:20: (lv_optional_7_0= 'optional' )
            	    // InternalPxManual.g:2208:10: lv_optional_7_0= 'optional'
            	    {
            	    lv_optional_7_0=(Token)match(input,42,FOLLOW_23); 

            	    										newLeafNode(lv_optional_7_0, grammarAccess.getKeywordAttributeAccess().getOptionalOptionalKeyword_3_2_0());
            	    									

            	    										if (current==null) {
            	    											current = createModelElement(grammarAccess.getKeywordAttributeRule());
            	    										}
            	    										setWithLastConsumed(current, "optional", true, "optional");
            	    									

            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalPxManual.g:2225:4: ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) )
            	    {
            	    // InternalPxManual.g:2225:4: ({...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) ) )
            	    // InternalPxManual.g:2226:5: {...}? => ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "getUnorderedGroupHelper().canSelect(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3)");
            	    }
            	    // InternalPxManual.g:2226:113: ( ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) ) )
            	    // InternalPxManual.g:2227:6: ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3(), 3);
            	    					
            	    // InternalPxManual.g:2230:9: ({...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) ) )
            	    // InternalPxManual.g:2230:10: {...}? => (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleKeywordAttribute", "true");
            	    }
            	    // InternalPxManual.g:2230:19: (otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) ) )
            	    // InternalPxManual.g:2230:20: otherlv_8= 'possibleValues:' ( (lv_possibleValues_9_0= ruleDocBody ) )
            	    {
            	    otherlv_8=(Token)match(input,43,FOLLOW_3); 

            	    									newLeafNode(otherlv_8, grammarAccess.getKeywordAttributeAccess().getPossibleValuesKeyword_3_3_0());
            	    								
            	    // InternalPxManual.g:2234:9: ( (lv_possibleValues_9_0= ruleDocBody ) )
            	    // InternalPxManual.g:2235:10: (lv_possibleValues_9_0= ruleDocBody )
            	    {
            	    // InternalPxManual.g:2235:10: (lv_possibleValues_9_0= ruleDocBody )
            	    // InternalPxManual.g:2236:11: lv_possibleValues_9_0= ruleDocBody
            	    {

            	    											newCompositeNode(grammarAccess.getKeywordAttributeAccess().getPossibleValuesDocBodyParserRuleCall_3_3_1_0());
            	    										
            	    pushFollow(FOLLOW_23);
            	    lv_possibleValues_9_0=ruleDocBody();

            	    state._fsp--;


            	    											if (current==null) {
            	    												current = createModelElementForParent(grammarAccess.getKeywordAttributeRule());
            	    											}
            	    											set(
            	    												current,
            	    												"possibleValues",
            	    												lv_possibleValues_9_0,
            	    												"org.pragmaticmodeling.pxmanual.PxManual.DocBody");
            	    											afterParserOrEnumRuleCall();
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getKeywordAttributeAccess().getUnorderedGroup_3());
            				

            }

            otherlv_10=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getKeywordAttributeAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKeywordAttribute"


    // $ANTLR start "ruleVerticalAlignment"
    // InternalPxManual.g:2274:1: ruleVerticalAlignment returns [Enumerator current=null] : ( (enumLiteral_0= 'top' ) | (enumLiteral_1= 'middle' ) | (enumLiteral_2= 'bottom' ) | (enumLiteral_3= 'baseline' ) ) ;
    public final Enumerator ruleVerticalAlignment() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalPxManual.g:2280:2: ( ( (enumLiteral_0= 'top' ) | (enumLiteral_1= 'middle' ) | (enumLiteral_2= 'bottom' ) | (enumLiteral_3= 'baseline' ) ) )
            // InternalPxManual.g:2281:2: ( (enumLiteral_0= 'top' ) | (enumLiteral_1= 'middle' ) | (enumLiteral_2= 'bottom' ) | (enumLiteral_3= 'baseline' ) )
            {
            // InternalPxManual.g:2281:2: ( (enumLiteral_0= 'top' ) | (enumLiteral_1= 'middle' ) | (enumLiteral_2= 'bottom' ) | (enumLiteral_3= 'baseline' ) )
            int alt29=4;
            switch ( input.LA(1) ) {
            case 44:
                {
                alt29=1;
                }
                break;
            case 45:
                {
                alt29=2;
                }
                break;
            case 46:
                {
                alt29=3;
                }
                break;
            case 47:
                {
                alt29=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalPxManual.g:2282:3: (enumLiteral_0= 'top' )
                    {
                    // InternalPxManual.g:2282:3: (enumLiteral_0= 'top' )
                    // InternalPxManual.g:2283:4: enumLiteral_0= 'top'
                    {
                    enumLiteral_0=(Token)match(input,44,FOLLOW_2); 

                    				current = grammarAccess.getVerticalAlignmentAccess().getTopEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getVerticalAlignmentAccess().getTopEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalPxManual.g:2290:3: (enumLiteral_1= 'middle' )
                    {
                    // InternalPxManual.g:2290:3: (enumLiteral_1= 'middle' )
                    // InternalPxManual.g:2291:4: enumLiteral_1= 'middle'
                    {
                    enumLiteral_1=(Token)match(input,45,FOLLOW_2); 

                    				current = grammarAccess.getVerticalAlignmentAccess().getMiddleEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getVerticalAlignmentAccess().getMiddleEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalPxManual.g:2298:3: (enumLiteral_2= 'bottom' )
                    {
                    // InternalPxManual.g:2298:3: (enumLiteral_2= 'bottom' )
                    // InternalPxManual.g:2299:4: enumLiteral_2= 'bottom'
                    {
                    enumLiteral_2=(Token)match(input,46,FOLLOW_2); 

                    				current = grammarAccess.getVerticalAlignmentAccess().getBottomEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getVerticalAlignmentAccess().getBottomEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalPxManual.g:2306:3: (enumLiteral_3= 'baseline' )
                    {
                    // InternalPxManual.g:2306:3: (enumLiteral_3= 'baseline' )
                    // InternalPxManual.g:2307:4: enumLiteral_3= 'baseline'
                    {
                    enumLiteral_3=(Token)match(input,47,FOLLOW_2); 

                    				current = grammarAccess.getVerticalAlignmentAccess().getBaselineEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getVerticalAlignmentAccess().getBaselineEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVerticalAlignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000001800007010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000003DE0358010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000001800020012L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000C04000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000001008000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000002008000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x000000181CC07010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000F00000000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008C00002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000001800000050L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000001800000010L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000001A00007010L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000001000000010L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000003C500008000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x00000C4500008000L});

}
