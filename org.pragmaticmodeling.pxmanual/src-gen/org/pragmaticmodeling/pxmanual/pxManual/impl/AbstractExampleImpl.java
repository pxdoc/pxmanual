/**
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxmanual.pxManual.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.pragmaticmodeling.pxmanual.pxManual.AbstractExample;
import org.pragmaticmodeling.pxmanual.pxManual.DocBody;
import org.pragmaticmodeling.pxmanual.pxManual.PxManualPackage;
import org.pragmaticmodeling.pxmanual.pxManual.Text;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Example</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.pragmaticmodeling.pxmanual.pxManual.impl.AbstractExampleImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxmanual.pxManual.impl.AbstractExampleImpl#getScreenshot <em>Screenshot</em>}</li>
 *   <li>{@link org.pragmaticmodeling.pxmanual.pxManual.impl.AbstractExampleImpl#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AbstractExampleImpl extends ManualElementImpl implements AbstractExample
{
  /**
   * The cached value of the '{@link #getTitle() <em>Title</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTitle()
   * @generated
   * @ordered
   */
  protected Text title;

  /**
   * The default value of the '{@link #getScreenshot() <em>Screenshot</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScreenshot()
   * @generated
   * @ordered
   */
  protected static final String SCREENSHOT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getScreenshot() <em>Screenshot</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScreenshot()
   * @generated
   * @ordered
   */
  protected String screenshot = SCREENSHOT_EDEFAULT;

  /**
   * The cached value of the '{@link #getBody() <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBody()
   * @generated
   * @ordered
   */
  protected DocBody body;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AbstractExampleImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PxManualPackage.Literals.ABSTRACT_EXAMPLE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Text getTitle()
  {
    return title;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTitle(Text newTitle, NotificationChain msgs)
  {
    Text oldTitle = title;
    title = newTitle;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PxManualPackage.ABSTRACT_EXAMPLE__TITLE, oldTitle, newTitle);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTitle(Text newTitle)
  {
    if (newTitle != title)
    {
      NotificationChain msgs = null;
      if (title != null)
        msgs = ((InternalEObject)title).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PxManualPackage.ABSTRACT_EXAMPLE__TITLE, null, msgs);
      if (newTitle != null)
        msgs = ((InternalEObject)newTitle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PxManualPackage.ABSTRACT_EXAMPLE__TITLE, null, msgs);
      msgs = basicSetTitle(newTitle, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PxManualPackage.ABSTRACT_EXAMPLE__TITLE, newTitle, newTitle));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getScreenshot()
  {
    return screenshot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setScreenshot(String newScreenshot)
  {
    String oldScreenshot = screenshot;
    screenshot = newScreenshot;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PxManualPackage.ABSTRACT_EXAMPLE__SCREENSHOT, oldScreenshot, screenshot));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocBody getBody()
  {
    return body;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBody(DocBody newBody, NotificationChain msgs)
  {
    DocBody oldBody = body;
    body = newBody;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PxManualPackage.ABSTRACT_EXAMPLE__BODY, oldBody, newBody);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBody(DocBody newBody)
  {
    if (newBody != body)
    {
      NotificationChain msgs = null;
      if (body != null)
        msgs = ((InternalEObject)body).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PxManualPackage.ABSTRACT_EXAMPLE__BODY, null, msgs);
      if (newBody != null)
        msgs = ((InternalEObject)newBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PxManualPackage.ABSTRACT_EXAMPLE__BODY, null, msgs);
      msgs = basicSetBody(newBody, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, PxManualPackage.ABSTRACT_EXAMPLE__BODY, newBody, newBody));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case PxManualPackage.ABSTRACT_EXAMPLE__TITLE:
        return basicSetTitle(null, msgs);
      case PxManualPackage.ABSTRACT_EXAMPLE__BODY:
        return basicSetBody(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case PxManualPackage.ABSTRACT_EXAMPLE__TITLE:
        return getTitle();
      case PxManualPackage.ABSTRACT_EXAMPLE__SCREENSHOT:
        return getScreenshot();
      case PxManualPackage.ABSTRACT_EXAMPLE__BODY:
        return getBody();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case PxManualPackage.ABSTRACT_EXAMPLE__TITLE:
        setTitle((Text)newValue);
        return;
      case PxManualPackage.ABSTRACT_EXAMPLE__SCREENSHOT:
        setScreenshot((String)newValue);
        return;
      case PxManualPackage.ABSTRACT_EXAMPLE__BODY:
        setBody((DocBody)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case PxManualPackage.ABSTRACT_EXAMPLE__TITLE:
        setTitle((Text)null);
        return;
      case PxManualPackage.ABSTRACT_EXAMPLE__SCREENSHOT:
        setScreenshot(SCREENSHOT_EDEFAULT);
        return;
      case PxManualPackage.ABSTRACT_EXAMPLE__BODY:
        setBody((DocBody)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case PxManualPackage.ABSTRACT_EXAMPLE__TITLE:
        return title != null;
      case PxManualPackage.ABSTRACT_EXAMPLE__SCREENSHOT:
        return SCREENSHOT_EDEFAULT == null ? screenshot != null : !SCREENSHOT_EDEFAULT.equals(screenshot);
      case PxManualPackage.ABSTRACT_EXAMPLE__BODY:
        return body != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (screenshot: ");
    result.append(screenshot);
    result.append(')');
    return result.toString();
  }

} //AbstractExampleImpl
