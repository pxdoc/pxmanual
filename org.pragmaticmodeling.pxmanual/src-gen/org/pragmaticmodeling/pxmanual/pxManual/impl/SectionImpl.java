/**
 * generated by Xtext 2.11.0
 */
package org.pragmaticmodeling.pxmanual.pxManual.impl;

import org.eclipse.emf.ecore.EClass;

import org.pragmaticmodeling.pxmanual.pxManual.PxManualPackage;
import org.pragmaticmodeling.pxmanual.pxManual.Section;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SectionImpl extends AbstractSectionImpl implements Section
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SectionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return PxManualPackage.Literals.SECTION;
  }

} //SectionImpl
