package org.pragmaticmodeling.pxmanual.generator;

import java.util.Set;

import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IOutputConfigurationProvider;
import org.eclipse.xtext.generator.OutputConfiguration;

import com.google.common.collect.Sets;

public class PxManualOutputConfigurationsProvider implements IOutputConfigurationProvider {

	@Override
	public Set<OutputConfiguration> getOutputConfigurations() {
		OutputConfiguration defaultOutput = new OutputConfiguration(IFileSystemAccess.DEFAULT_OUTPUT);
		defaultOutput.setDescription("Output Folder");
		defaultOutput.setOutputDirectory("./src-gen");
		defaultOutput.setOverrideExistingResources(true);
		defaultOutput.setCreateOutputDirectory(true);
		defaultOutput.setCleanUpDerivedResources(true);
		defaultOutput.setSetDerivedProperty(true);
		defaultOutput.setKeepLocalHistory(true);
		OutputConfiguration customOutput = new OutputConfiguration("Src_Dir");
		customOutput.setDescription("Custom Output Folder");
		customOutput.setOutputDirectory("./src");
		customOutput.setOverrideExistingResources(false);
		customOutput.setCreateOutputDirectory(true);
		customOutput.setCleanUpDerivedResources(false);
		customOutput.setSetDerivedProperty(false);
		customOutput.setKeepLocalHistory(true);
		OutputConfiguration rootOutput = new OutputConfiguration("Root_Dir");
		rootOutput.setDescription("Root Output Folder");
		rootOutput.setOutputDirectory(".");
		rootOutput.setOverrideExistingResources(true);
		rootOutput.setCreateOutputDirectory(false);
		rootOutput.setCleanUpDerivedResources(false);
		rootOutput.setSetDerivedProperty(false);
		rootOutput.setKeepLocalHistory(true);
		OutputConfiguration eclipseOutput = new OutputConfiguration("Eclipse_Dir");
		eclipseOutput.setDescription("Eclipse Documentation Output Folder");
		eclipseOutput.setOutputDirectory("./eclipse");
		eclipseOutput.setOverrideExistingResources(true);
		eclipseOutput.setCreateOutputDirectory(true);
		eclipseOutput.setCleanUpDerivedResources(true);
		eclipseOutput.setSetDerivedProperty(false);
		eclipseOutput.setKeepLocalHistory(true);
		return Sets.newHashSet(customOutput,defaultOutput, rootOutput, eclipseOutput);
	}

}
