package org.pragmaticmodeling.pxmanual.util;

import com.google.common.collect.Iterables;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxmanual.pxManual.Document;
import org.pragmaticmodeling.pxmanual.pxManual.Page;

@SuppressWarnings("all")
public class PxManualHelper {
  public static List<Page> getPages(final Document doc) {
    return IterableExtensions.<Page>toList(Iterables.<Page>filter(doc.getBody().getElements(), Page.class));
  }
  
  public static Document getParentDocument(final EObject eObject) {
    return EcoreUtil2.<Document>getContainerOfType(eObject, Document.class);
  }
  
  public static String title(final EObject eObject) {
    boolean _matched = false;
    if (eObject instanceof Document) {
      _matched=true;
      return ((Document)eObject).getTitle().getBody();
    }
    if (!_matched) {
      if (eObject instanceof Page) {
        _matched=true;
        return ((Page)eObject).getTitle().getBody();
      }
    }
    return "FIXME";
  }
}
