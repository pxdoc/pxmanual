package org.pragmaticmodeling.pxmanual.gen.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxgen.runtime.IContext;
import org.pragmaticmodeling.pxmanual.gen.IDocumentationProjectContext;

@SuppressWarnings("all")
public class PluginXmlFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IDocumentationProjectContext context;
  
  public PluginXmlFile(final IDocumentationProjectContext context) {
    super();
    this.context = context;
  }
  
  public IDocumentationProjectContext getContext() {
    return this.context;
  }
  
  public void setContext(final IDocumentationProjectContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<?eclipse version=\"3.4\"?>");
    _builder.newLine();
    _builder.append("<plugin>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("   ");
    _builder.append("<extension");
    _builder.newLine();
    _builder.append("         ");
    _builder.append("point=\"org.eclipse.help.toc\">");
    _builder.newLine();
    _builder.append("      ");
    _builder.append("<toc");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("file=\"toc.xml\"");
    _builder.newLine();
    _builder.append("            ");
    _builder.append("primary=\"true\">");
    _builder.newLine();
    _builder.append("      ");
    _builder.append("</toc>");
    _builder.newLine();
    _builder.append("   ");
    _builder.append("</extension>");
    _builder.newLine();
    _builder.newLine();
    _builder.append("</plugin>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    return _builder.toString();
  }
}
