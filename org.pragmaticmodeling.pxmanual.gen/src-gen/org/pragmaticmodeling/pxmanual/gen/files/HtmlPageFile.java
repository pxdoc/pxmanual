package org.pragmaticmodeling.pxmanual.gen.files;

import com.google.inject.Injector;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxgen.runtime.IContext;
import org.pragmaticmodeling.pxmanual.gen.IPxManualsContext;
import org.pragmaticmodeling.pxmanual.pxManual.ManualElement;
import org.pragmaticmodeling.pxmanual.util.PxManualHelper;

@SuppressWarnings("all")
public class HtmlPageFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IPxManualsContext context;
  
  public HtmlPageFile(final IPxManualsContext context, final EObject model) {
    super();
    this.context = context;
    this.model = model;
  }
  
  public IPxManualsContext getContext() {
    return this.context;
  }
  
  public void setContext(final IPxManualsContext context) {
    this.context = context;
  }
  
  private EObject model;
  
  public EObject getModel() {
    return this.model;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<!DOCTYPE html>");
    _builder.newLine();
    _builder.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en-gb\" lang=\"en-gb\" dir=\"ltr\">");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<head>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<title>");
    String _title = PxManualHelper.title(this.model);
    _builder.append(_title, "\t\t");
    _builder.append("</title>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("<link rel=\"stylesheet\" href=\"resources/documentation.css\" type=\"text/css\"/>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<link href=\"share/syntaxhighlighter/shCore.css\" rel=\"stylesheet\" type=\"text/css\" />");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<link href=\"share/syntaxhighlighter/pxdoc.css\" rel=\"stylesheet\" type=\"text/css\" />");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<script src=\"share/syntaxhighlighter/shCore.js\" type=\"text/javascript\"></script>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<script src=\"share/syntaxhighlighter/syntaxhighlighter.js\" type=\"text/javascript\"></script>");
    _builder.newLine();
    String _localStyles = this.context.localStyles(PxManualHelper.getParentDocument(this.model));
    _builder.append(_localStyles);
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</head>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<body>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<h");
    int _startingHeadingLevel = this.context.startingHeadingLevel(PxManualHelper.getParentDocument(this.model));
    _builder.append(_startingHeadingLevel, "\t");
    _builder.append(">");
    String _title_1 = PxManualHelper.title(this.model);
    _builder.append(_title_1, "\t");
    _builder.append("</h");
    int _startingHeadingLevel_1 = this.context.startingHeadingLevel(PxManualHelper.getParentDocument(this.model));
    _builder.append(_startingHeadingLevel_1, "\t");
    _builder.append(">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _c = this.context.toc(this.model);
    _builder.append(_c, "\t");
    _builder.newLineIfNotEmpty();
    {
      List<ManualElement> _elements = this.context.getElements(this.model);
      for(final ManualElement e : _elements) {
        _builder.append("\t");
        String _html = this.context.toHtml(e, true);
        _builder.append(_html, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("</body>");
    _builder.newLine();
    _builder.append("</html>");
    return _builder.toString();
  }
}
