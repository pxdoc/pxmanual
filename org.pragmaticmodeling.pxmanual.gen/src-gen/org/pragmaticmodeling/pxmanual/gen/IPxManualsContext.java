package org.pragmaticmodeling.pxmanual.gen;

import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxmanual.pxManual.AbstractSection;
import org.pragmaticmodeling.pxmanual.pxManual.Cell;
import org.pragmaticmodeling.pxmanual.pxManual.DocBody;
import org.pragmaticmodeling.pxmanual.pxManual.Document;
import org.pragmaticmodeling.pxmanual.pxManual.Example;
import org.pragmaticmodeling.pxmanual.pxManual.Image;
import org.pragmaticmodeling.pxmanual.pxManual.Keyword;
import org.pragmaticmodeling.pxmanual.pxManual.KeywordAttribute;
import org.pragmaticmodeling.pxmanual.pxManual.ManualElement;
import org.pragmaticmodeling.pxmanual.pxManual.Page;
import org.pragmaticmodeling.pxmanual.pxManual.Row;
import org.pragmaticmodeling.pxmanual.pxManual.Section;
import org.pragmaticmodeling.pxmanual.pxManual.Table;
import org.pragmaticmodeling.pxmanual.pxManual.Text;
import org.pragmaticmodeling.pxmanual.pxManual.Usage;

@SuppressWarnings("all")
public interface IPxManualsContext {
  @PxGenParameter
  public abstract String getBasePackage();
  
  @PxGenParameter
  public abstract void setBasePackage(final String basePackage);
  
  @PxGenParameter
  public abstract String getPxManualUri();
  
  @PxGenParameter
  public abstract void setPxManualUri(final String pxManualUri);
  
  @PxGenParameter
  public abstract void setDocument(final Document document);
  
  @PxGenParameter
  public abstract Integer getDepth();
  
  @PxGenParameter
  public abstract void setDepth(final Integer depth);
  
  @PxGenParameter
  public abstract XtextResourceSet getResourceSet();
  
  @PxGenParameter
  public abstract void setResourceSet(final XtextResourceSet resourceSet);
  
  public abstract String toHtml(final ManualElement e, final boolean withGutter);
  
  public abstract String toUsageExample(final Usage usage);
  
  public abstract String toAttributeRow(final KeywordAttribute attribute);
  
  public abstract String toBasicExample(final Example example);
  
  public abstract String toHtml(final DocBody docBody);
  
  public abstract String toHtmlDocBody(final DocBody docBody, final boolean withGutter);
  
  public abstract String toHtmlDocumentation(final Text documentation, final boolean withGutter);
  
  public abstract String toPageLink(final Page page);
  
  public abstract String toHtmlTable(final Table table);
  
  public abstract String toHtmlRow(final Row row);
  
  public abstract String toHtmlCell(final Cell cell);
  
  public abstract String toHtmlImage(final Image image);
  
  public abstract String getDocName(final EObject object);
  
  public abstract String imageWidth(final Image image);
  
  public abstract String toHtmlSection(final Section section);
  
  public abstract String leaveSection();
  
  public abstract String enterSection();
  
  public abstract String toHtmlKeyword(final Keyword keyword);
  
  public abstract List<ManualElement> getElements(final EObject object);
  
  /**
   * Table of contents
   */
  public abstract String toToc(final ManualElement object);
  
  public abstract String toc(final EObject object);
  
  public abstract String getId(final AbstractSection section);
  
  public abstract String getId(final Keyword keyword);
  
  public abstract String getHref(final AbstractSection section);
  
  public abstract String getHtmlPage(final EObject object);
  
  public abstract String fileName(final Document document);
  
  public abstract String fileName(final Page page);
  
  public abstract String getHref(final Keyword keyword);
  
  public abstract boolean hasSubTitles(final AbstractSection section);
  
  public abstract Boolean isTitle(final ManualElement element);
  
  public abstract String title(final Keyword keyword);
  
  public abstract Document getDocument();
  
  public abstract Document loadDocument();
  
  public abstract String localStyles(final Document document);
  
  public abstract int startingHeadingLevel(final Document document);
}
