package org.pragmaticmodeling.pxmanual.gen;

import org.pragmaticmodeling.pxgen.runtime.AbstractStandaloneSetup;

@SuppressWarnings("all")
public class PxManualsStandaloneSetup extends AbstractStandaloneSetup {
  public PxManualsStandaloneSetup() {
    super(new org.pragmaticmodeling.pxmanual.gen.PxManualsModule());
  }
}
