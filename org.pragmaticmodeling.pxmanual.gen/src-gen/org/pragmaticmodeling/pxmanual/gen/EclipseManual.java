package org.pragmaticmodeling.pxmanual.gen;

import com.google.inject.Injector;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.pragmaticmodeling.pxgen.runtime.AbstractGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;
import org.pragmaticmodeling.pxmanual.gen.DocumentationProject;
import org.pragmaticmodeling.pxmanual.gen.IEclipseManualContext;
import org.pragmaticmodeling.pxmanual.gen.IPxManualsContext;
import org.pragmaticmodeling.pxmanual.pxManual.AbstractSection;
import org.pragmaticmodeling.pxmanual.pxManual.Cell;
import org.pragmaticmodeling.pxmanual.pxManual.DocBody;
import org.pragmaticmodeling.pxmanual.pxManual.Document;
import org.pragmaticmodeling.pxmanual.pxManual.Example;
import org.pragmaticmodeling.pxmanual.pxManual.Image;
import org.pragmaticmodeling.pxmanual.pxManual.Keyword;
import org.pragmaticmodeling.pxmanual.pxManual.KeywordAttribute;
import org.pragmaticmodeling.pxmanual.pxManual.ManualElement;
import org.pragmaticmodeling.pxmanual.pxManual.Page;
import org.pragmaticmodeling.pxmanual.pxManual.Row;
import org.pragmaticmodeling.pxmanual.pxManual.Section;
import org.pragmaticmodeling.pxmanual.pxManual.Table;
import org.pragmaticmodeling.pxmanual.pxManual.Text;
import org.pragmaticmodeling.pxmanual.pxManual.Usage;
import org.pragmaticmodeling.pxmanual.util.PxManualHelper;

@SuppressWarnings("all")
public class EclipseManual extends AbstractGeneratorFragment implements IEclipseManualContext {
  public EclipseManual() {
    super();
  }
  
  public IPxManualsContext getParentContext() {
    return (IPxManualsContext)getParentFragment();
  }
  
  private String projectName;
  
  @PxGenParameter
  public String getProjectName() {
    return projectName;
  }
  
  @PxGenParameter
  public void setProjectName(final String projectName) {
    this.projectName = projectName;
  }
  
  private DocumentationProject documentationProject;
  
  public DocumentationProject getDocumentationProject() {
    if (documentationProject == null) {
    	this.documentationProject = new DocumentationProject();
    	this.documentationProject.setProjectModelName("DocumentationProject");
    	this.documentationProject.setParentFragment(this);
    }
    return documentationProject;
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxManualUri(final String pxManualUri) {
    getParentContext().setPxManualUri(pxManualUri);
  }
  
  @PxGenParameter
  public String getPxManualUri() {
    return getParentContext().getPxManualUri();
  }
  
  @PxGenParameter
  public void setDocument(final Document document) {
    getParentContext().setDocument(document);
  }
  
  @PxGenParameter
  public void setDepth(final Integer depth) {
    getParentContext().setDepth(depth);
  }
  
  @PxGenParameter
  public Integer getDepth() {
    return getParentContext().getDepth();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  public String toHtml(final ManualElement e, final boolean withGutter) {
    return getParentContext().toHtml(e, withGutter);
  }
  
  public String toUsageExample(final Usage usage) {
    return getParentContext().toUsageExample(usage);
  }
  
  public String toAttributeRow(final KeywordAttribute attribute) {
    return getParentContext().toAttributeRow(attribute);
  }
  
  public String toBasicExample(final Example example) {
    return getParentContext().toBasicExample(example);
  }
  
  public String toHtml(final DocBody docBody) {
    return getParentContext().toHtml(docBody);
  }
  
  public String toHtmlDocBody(final DocBody docBody, final boolean withGutter) {
    return getParentContext().toHtmlDocBody(docBody, withGutter);
  }
  
  public String toHtmlDocumentation(final Text documentation, final boolean withGutter) {
    return getParentContext().toHtmlDocumentation(documentation, withGutter);
  }
  
  public String toPageLink(final Page page) {
    return getParentContext().toPageLink(page);
  }
  
  public String toHtmlTable(final Table table) {
    return getParentContext().toHtmlTable(table);
  }
  
  public String toHtmlRow(final Row row) {
    return getParentContext().toHtmlRow(row);
  }
  
  public String toHtmlCell(final Cell cell) {
    return getParentContext().toHtmlCell(cell);
  }
  
  public String toHtmlImage(final Image image) {
    return getParentContext().toHtmlImage(image);
  }
  
  public String getDocName(final EObject object) {
    return getParentContext().getDocName(object);
  }
  
  public String imageWidth(final Image image) {
    return getParentContext().imageWidth(image);
  }
  
  public String toHtmlSection(final Section section) {
    return getParentContext().toHtmlSection(section);
  }
  
  public String leaveSection() {
    return getParentContext().leaveSection();
  }
  
  public String enterSection() {
    return getParentContext().enterSection();
  }
  
  public String toHtmlKeyword(final Keyword keyword) {
    return getParentContext().toHtmlKeyword(keyword);
  }
  
  public List<ManualElement> getElements(final EObject object) {
    return getParentContext().getElements(object);
  }
  
  /**
   * Table of contents
   */
  public String toToc(final ManualElement object) {
    return getParentContext().toToc(object);
  }
  
  public String toc(final EObject object) {
    return getParentContext().toc(object);
  }
  
  public String getId(final AbstractSection section) {
    return getParentContext().getId(section);
  }
  
  public String getId(final Keyword keyword) {
    return getParentContext().getId(keyword);
  }
  
  public String getHref(final AbstractSection section) {
    return getParentContext().getHref(section);
  }
  
  public String getHtmlPage(final EObject object) {
    return getParentContext().getHtmlPage(object);
  }
  
  public String fileName(final Document document) {
    return getParentContext().fileName(document);
  }
  
  public String fileName(final Page page) {
    return getParentContext().fileName(page);
  }
  
  public String getHref(final Keyword keyword) {
    return getParentContext().getHref(keyword);
  }
  
  public boolean hasSubTitles(final AbstractSection section) {
    return getParentContext().hasSubTitles(section);
  }
  
  public Boolean isTitle(final ManualElement element) {
    return getParentContext().isTitle(element);
  }
  
  public String title(final Keyword keyword) {
    return getParentContext().title(keyword);
  }
  
  public Document getDocument() {
    return getParentContext().getDocument();
  }
  
  public Document loadDocument() {
    return getParentContext().loadDocument();
  }
  
  public String localStyles(final Document document) {
    return getParentContext().localStyles(document);
  }
  
  public int startingHeadingLevel(final Document document) {
    return getParentContext().startingHeadingLevel(document);
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    addProject(getDocumentationProject());
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
  }
  
  @Override
  public void doGenerate() {
    {
      this.documentationProject.setWithPluginXml(true);
      List<String> _buildBinIncludes = this.documentationProject.getBuildBinIncludes();
      _buildBinIncludes.add("html");
      List<String> _buildBinIncludes_1 = this.documentationProject.getBuildBinIncludes();
      _buildBinIncludes_1.add("toc.xml");
      Document doc = this.getDocument();
      // generate file Toc
      getDocumentationProject().getRoot().generateFile("toc.xml", new org.pragmaticmodeling.pxmanual.gen.files.TocFile(getDocumentationProject(), doc).getContent());
      // generate file PluginXml
      getDocumentationProject().getRoot().generateFile("plugin.xml", new org.pragmaticmodeling.pxmanual.gen.files.PluginXmlFile(getDocumentationProject()).getContent());
      // generate file HtmlPage
      getDocumentationProject().getHtml().generateFile((this.fileName(doc) + ".html"), new org.pragmaticmodeling.pxmanual.gen.files.HtmlPageFile(this, doc).getContent());
      List<Page> _pages = PxManualHelper.getPages(doc);
      for (final Page page : _pages) {
        // generate file HtmlPage
        getDocumentationProject().getHtml().generateFile((this.fileName(page) + ".html"), new org.pragmaticmodeling.pxmanual.gen.files.HtmlPageFile(this, page).getContent());
      }
    }
  }
}
