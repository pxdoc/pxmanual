package org.pragmaticmodeling.pxmanual.gen;

import org.pragmaticmodeling.pxmanual.gen.IEclipseManualContext;

@SuppressWarnings("all")
public interface IDocumentationProjectContext extends IEclipseManualContext {
}
